<?php

return array(

    // 'ios_dev'     => array(
    //     'environment' => 'development',
    //     'certificate' => storage_path('certificates/development.pem'),
    //     'passPhrase'  => env('IOS_PASS', ''),
    //     'service'     => 'apns'
    // ),
    // 'ios'     => array(
    //     'environment' => 'production',
    //     'certificate' => storage_path('certificates/production.pem'),
    //     'passPhrase'  => env('IOS_PASS', ''),
    //     'service'     => 'apns'
    // ),

    'android' => array(
        'environment' => 'production',
        'apiKey'      => env('FCM_API_KEY', ''),
        'service'     => 'gcm',
    )

);
