let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/assets/front/js/app.js', 'public/assets/front.js')
mix.sass('resources/assets/front/sass/app.scss', 'public/assets/front.css')

mix.js('resources/assets/panel/js/app.js', 'public/assets/panel.js')
mix.sass('resources/assets/panel/sass/app.scss', 'public/assets/panel.css')

/* dx themes */
if (mix.inProduction()) {
    // mix.sass('resources/assets/panel/sass/dx/carmine.scss', 'public/assets/themes/carmine.css');
    // mix.sass('resources/assets/panel/sass/dx/carmine.compact.scss', 'public/assets/themes/carmine.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/contrast.scss', 'public/assets/themes/contrast.css');
    // mix.sass('resources/assets/panel/sass/dx/contrast.compact.scss', 'public/assets/themes/contrast.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/dark.scss', 'public/assets/themes/dark.css');
    // mix.sass('resources/assets/panel/sass/dx/dark.compact.scss', 'public/assets/themes/dark.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/darkmoon.scss', 'public/assets/themes/darkmoon.css');
    // mix.sass('resources/assets/panel/sass/dx/darkmoon.compact.scss', 'public/assets/themes/darkmoon.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/darkviolet.scss', 'public/assets/themes/darkviolet.css');
    // mix.sass('resources/assets/panel/sass/dx/darkviolet.compact.scss', 'public/assets/themes/darkviolet.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/greenmist.scss', 'public/assets/themes/greenmist.css');
    // mix.sass('resources/assets/panel/sass/dx/greenmist.compact.scss', 'public/assets/themes/greenmist.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/light.scss', 'public/assets/themes/light.css');
    // mix.sass('resources/assets/panel/sass/dx/light.compact.scss', 'public/assets/themes/light.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/softblue.scss', 'public/assets/themes/softblue.css');
    // mix.sass('resources/assets/panel/sass/dx/softblue.compact.scss', 'public/assets/themes/softblue.compact.css');

    // mix.sass('resources/assets/panel/sass/dx/android5.light.scss', 'public/assets/themes/android5.light.css');
    // mix.sass('resources/assets/panel/sass/dx/ios7.default.scss', 'public/assets/themes/ios7.default.css');

    mix.version()
}