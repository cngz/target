<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 100)->nullable();
            $table->unsignedInteger('school_id')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('driver_id')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('image')->nullable();
            $table->string('signature')->nullable();
            $table->string('desc')->nullable();
            $table->unsignedInteger('order')->nullable()->default(0);
            $table->timestamp('create_date')->useCurrent();
            $table->date('h2s_date')->nullable();
            $table->date('s2h_date')->nullable();

            $table->foreign('school_id')->references('id')->on('schools')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('parent_id')->references('id')->on('parents')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('driver_id')->references('id')->on('drivers')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
