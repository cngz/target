<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('school_id')->nullable();
            $table->string('carno')->nullable();
            $table->string('carname')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('desc')->nullable();
            $table->string('password');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->enum('device_type', ['android', 'ios', 'win'])->nullable();
            $table->string('device_token')->nullable();
            $table->string('api_token')->nullable();
            $table->timestamp('api_date')->nullable();
            $table->timestamp('create_date')->useCurrent();

            $table->foreign('school_id')->references('id')->on('schools')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
