<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('desc')->nullable();
            $table->enum('device_type', ['android', 'ios', 'win'])->nullable();
            $table->string('device_token')->nullable();
            $table->string('api_token')->nullable();
            $table->timestamp('api_date')->nullable();
            $table->timestamp('create_date')->useCurrent();
            $table->timestamp('start_date')->nullable()->comment('agreement start date');
            $table->timestamp('end_date')->nullable()->comment('agreement end date');
            $table->decimal('fare', 7, 2)->default(0.00)->comment('agreement amount');
            $table->decimal('payments_total', 7, 2)->default(0.00)->comment('total paid amount during agreement dates');
            $table->decimal('balance', 7, 2)->default(0.00)->comment('balance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents');
    }
}
