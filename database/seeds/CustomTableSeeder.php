<?php

use Illuminate\Database\Seeder;

use Models\Parent\Parentt;

class CustomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $parents = Parentt::all();

        foreach ($parents as $parent) {
            $parent->payments()->create([
                'amount' => 100,
                'date'   => now(),
            ]);
        }
    }
}
