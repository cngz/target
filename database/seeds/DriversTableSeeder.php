<?php

use Illuminate\Database\Seeder;

use Models\School\School;

class DriversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $school  = School::first();
        $schools = School::skip(1)->take(1000)->get();

        $school->drivers()->create([
            'carno'   => 'B1321A',
            'carname' => 'Mercedez Benz',
            'name'    => 'Driver 1',
            'phone'   => '555705050',
            'lat'     => 42.859499,
            'lng'     => 74.600349,
        ]);

        foreach ($schools as $school) {
            $rand = rand(3,6);
            $drivers = [];
            for($i = 0; $i < $rand; $i++) {
                $drivers[] = [
                    'carno'   => 'B'.rand(1000, 9999).array_random(['A', 'B', 'C', 'D']),
                    'carname' => array_random(['BMW', 'Mercedez', 'Ford']),
                    'name'    => $faker->name,
                    'phone'   => array_random(['550', '555', '700', '702', '772', '777']) . rand(100000, 999999),
                    'lat'     => $faker->latitude(42.8201, 42.9201),
                    'lng'     => $faker->latitude(74.5001, 74.6701),
                ];
            }

            $school->drivers()->createMany($drivers);
        }
    }
}
