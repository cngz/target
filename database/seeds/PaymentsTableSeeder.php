<?php

use Illuminate\Database\Seeder;

use Models\Parent\Parentt;
use Models\Payment\Payment;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $parents = Parentt::all();

        foreach ($parents as $parent) {
            $rand     = rand(2,6);
            $payments = [];
            for($i = 0; $i < $rand; $i++) {
                $payments[] = [
                    'amount' => rand(10000, 99999)/100,
                    'date'   => now()->addDays(-50, 50),
                ];
            }

            $parent->payments()->createMany($payments);
        }
    }
}
