<?php

use Illuminate\Database\Seeder;

use Models\User\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        User::create([
            'name'           => 'Admin 1',
            'phone'          => '555905050',
            'email'          => 'admin@gmail.com',
            'role'           => 'admin',
            'remember_token' => 'skdlfsjklfjslkdfjksldf9032kld',
            'active'         => 1,
        ]);

        User::create([
            'name'           => 'Manager 1',
            'phone'          => '555905060',
            'email'          => 'manager@gmail.com',
            'role'           => 'manager',
            'remember_token' => 'sdflkasfjkajl32kjdlskfkldsjlkdsjflsf',
            'active'         => 1,
        ]);

        for($i = 0; $i < 10; $i++)
        {
            User::create([
                'name'           => $faker->name,
                'phone'          => array_random(['550', '555', '700', '702', '772', '777']) . rand(100000, 999999),
                'email'          => $faker->unique()->email,
                'role'           => array_random(['manager', 'admin']),
            ]);
        }
    }
}
