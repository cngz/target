<?php

use Illuminate\Database\Seeder;

use Models\Parent\Parentt;

class ParentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        Parentt::create([
            'name'       => 'Parent 1',
            'phone'      => '555605050',
            'address'    => '12 Mkr 12-84',
            'fare'       => rand(100000, 3000000)/100,
            'start_date' => now()->subDays(100),
            'end_date'   => now()->addDays(100),
        ]);

        for($i = 0; $i < 30; $i++)
        {
            Parentt::create([
                'name'       => $faker->name,
                'phone'      => array_random(['550', '555', '700', '702', '772', '777']) . rand(100000, 999999),
                'address'    => $faker->unique()->streetAddress,
                'fare'       => rand(100000, 3000000)/100,
                'start_date' => now()->subDays(100),
                'end_date'   => now()->addDays(100),
            ]);
        }
    }
}
