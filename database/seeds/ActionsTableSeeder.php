<?php

use Illuminate\Database\Seeder;

use Models\Student\Student;
use Models\Driver\Driverr;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $students = Student::all();
        $drivers  = Driverr::all()->pluck('id')->toArray();

        foreach ($students as $student) {
            $rand = rand(4,12);
            $actions = [];
            for($i = 0; $i < $rand; $i++) {
                $actions[] = [
                    'driver_id' => array_random($drivers),
                    'type'      => array_random(['h2s', 's2h']),
                    'take_date' => now()->subDays(rand(1,20)),
                    'status'    => rand(0, 1),
                ];
            }

            $student->actions()->createMany($actions);
        }
    }
}
