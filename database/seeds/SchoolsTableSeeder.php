<?php

use Illuminate\Database\Seeder;

use Models\School\School;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create([
            'name'    => 'СШ №60',
            'address' => 'Ahunbaeva 34',
            'active'  => 1,
        ]);

        School::create([
            'name'    => 'СШ №54',
            'address' => 'Toktogul 223',
            'active'  => 1,
        ]);

        School::create([
            'name'    => 'СШ №5',
            'address' => 'Moskovskaya 34',
            'active'  => 1,
        ]);

        School::create([
            'name'    => 'СШ №22',
            'address' => '12 Mkr',
            'active'  => 1,
        ]);
    }
}
