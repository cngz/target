<?php

use Illuminate\Database\Seeder;

use Models\Parent\Parentt;
use Models\Driver\Driverr;
use Models\School\School;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $parents = Parentt::all();
        $drivers = Driverr::all()->pluck('id')->toArray();

        $schools = School::all()->pluck('id')->toArray();

        foreach ($parents as $parent) {
            $rand = rand(2,4);
            $students = [];
            for($i = 0; $i < $rand; $i++) {
                $students[] = [
                    'school_id' => array_random($schools),
                    'driver_id' => array_random($drivers),
                    'name'      => $faker->name,
                    'phone'     => array_random(['550', '555', '700', '702', '772', '777']) . rand(100000, 999999),
                    'image'     => 'https://randomuser.me/api/portraits/' . array_random(['men', 'women']) . '/' . rand(0, 40) . '.jpg',
                ];
            }

            $parent->children()->createMany($students);
        }
    }
}
