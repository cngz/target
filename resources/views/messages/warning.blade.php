@if(session()->has('warning-message'))
<div class="alert alert-warning {{ $class or '' }} {{ $dismissable or '' }}" role="alert">{{ session('warning-message') }}</div>
@endif
