@if(session()->has('danger-message'))
<div class="alert alert-danger {{ $class or '' }} {{ $dismissable or '' }}" role="alert">{{ session('danger-message') }}</div>
@endif
