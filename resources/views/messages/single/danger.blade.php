@if($errors->has($name))
<div class="alert alert-danger {{ $class or '' }} alert-dismissable" role="alert">{{ $errors->first($name) }}</div>
@endif
