@if(session()->has('info-message'))
<div class="alert alert-info {{ $class or '' }} {{ $dismissable or '' }}" role="alert">{{ session('info-message') }}</div>
@endif
