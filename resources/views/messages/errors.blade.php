@if($errors->any())
<div class="alert alert-danger {{ $class or '' }} {{ $dismissable or '' }}" role="role">
    @foreach($errors->all() as $error)
    <div>{{ $error }}</div>
    @endforeach
</div>
@endif
