@if(session()->has('success-message'))
<div class="alert alert-success {{ $class or '' }} {{ $dismissable or '' }}" role="alert">{{ session('success-message') }}</div>
@endif
