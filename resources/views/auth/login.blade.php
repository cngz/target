@extends('layouts.app')
@section('title', trans('site.Login'))
@section('content')
<div id="auth" class="text-center">
    <a href="{{ route('front.home') }}"><span class="fa fa-home fa-4x"></span></a>
    <div>&nbsp;</div>
    <form class="form-horizontal" method="POST" action="/login">
        {{ csrf_field() }}
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"><i class="fa fa-user"></i></span>
            </div>
            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}"
                required autofocus tabindex="1"
                placeholder="{{ trans('site.Enter email') }}"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter email') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white"><i class="fa fa-key"></i></span>
            </div>
            <input type="password" class="form-control" name="password" required tabindex="2"
                placeholder="{{ trans('site.Enter password') }}"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter password') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        {{-- <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white" style="padding: 0">
                    <img src="{{ route('front.captcha', 4) }}" style="height: 36px; width:90px">
                </span>
            </div>
            <input type="text" class="form-control" name="captcha" required tabindex="3"
                placeholder="{{ trans('site.Enter code') }}"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter code') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div> --}}

        <div class="form-group">
            <button class="btn btn-outline-primary btn-block text-uppercase" type="submit" tabindex="3">{{ trans('site.Login') }}</button>
            <a href="{{ route('password.request') }}" class="btn btn-link btn-block text-lowercase">{{ trans('site.Forgot password?') }}</a>
        </div>
    </form>
    <br>
    @include('messages.flash')
    <br>
</div>
@stop
@section('style')
<style type="text/css">
body {
    border-top: 4px solid #007bff;
}
#auth {
    width: 300px;
    margin: 20px auto;
}
</style>
@endsection