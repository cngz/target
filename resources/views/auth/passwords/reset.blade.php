@extends('layouts.app')
@section('title', trans('site.Login'))
@section('content')
<div id="auth" class="">
    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="form-group">
            <label class="">{{ trans('site.Enter email') }}</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                required autofocus tabindex="1"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter email') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Enter password') }}</label>
            <input type="password" class="form-control" name="password" required tabindex="2"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter password') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Confirm password') }}</label>
            <input type="password" class="form-control" name="password_confirmation" required tabindex="3"
                oninvalid="this.setCustomValidity('{{ trans('site.Confirm password') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <button class="btn btn-outline-primary btn-block text-uppercase" type="submit" tabindex="4">{{ trans('site.Save') }}</button>
        </div>
    </form>
    <br>
    @include('messages.flash')
    <br>
</div>
@stop
@section('style')
<style type="text/css">
body {
    border-top: 4px solid #007bff;
}
#auth {
    width: 300px;
    margin: 20px auto;
}
</style>
@endsection