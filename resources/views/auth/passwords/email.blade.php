@extends('layouts.app')
@section('title', trans('site.Login'))
@section('content')
<div id="auth">
    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="">{{ trans('site.Enter email') }}</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                required autofocus tabindex="1"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter email') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <button class="btn btn-outline-primary btn-block text-uppercase" type="submit" tabindex="3">{{ trans('site.Reset password') }}</button>
            <a href="{{ route('login') }}" class="btn btn-link btn-block text-lowercase">{{ trans('site.Back') }}</a>
        </div>
    </form>
    <br>
    @include('messages.flash')
    @if(session()->has('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
    @endif
    <br>
</div>
@stop
@section('style')
<style type="text/css">
body {
    border-top: 4px solid #007bff;
}
#auth {
    width: 300px;
    margin: 20px auto;
}
</style>
@endsection