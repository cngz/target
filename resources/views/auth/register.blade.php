@extends('layouts.app')
@section('title', trans('site.Login'))
@section('content')
<div id="auth">
    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label class="">{{ trans('site.Enter your email') }}</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                required autofocus tabindex="1"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter email') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Enter your name') }}</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                required autofocus tabindex="1"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter your name') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Enter your phone number') }}</label>
            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                required autofocus tabindex="1"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter your phone number') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Select gender') }}</label>
            <select id="gender" class="form-control" name="gender" value="{{ old('gender') }}"
                required autofocus tabindex="1"
                oninvalid="this.setCustomValidity('{{ trans('site.Select gender') }}')"
                oninput="this.setCustomValidity('')"
            >
                <option value="" disabled selected>{{ trans('site.Select gender') }}</option>
                <option value="m">{{ trans('site.Male') }}</option>
                <option value="f">{{ trans('site.Female') }}</option>
            </select>
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Enter your new password') }}</label>
            <input type="password" class="form-control" name="password" required tabindex="2"
                oninvalid="this.setCustomValidity('{{ trans('site.Enter your new password') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <label class="">{{ trans('site.Confirm your new password') }}</label>
            <input type="password" class="form-control" name="password_confirmation" required tabindex="2"
                oninvalid="this.setCustomValidity('{{ trans('site.Confirm your new password') }}')"
                oninput="this.setCustomValidity('')"
            >
        </div>

        <div class="form-group">
            <button class="btn btn-outline-primary btn-block text-uppercase" type="submit" tabindex="3">{{ trans('site.Register') }}</button>
            <a href="{{ route('login') }}" class="btn btn-link btn-block text-lowercase">{{ trans('site.Back') }}</a>
        </div>
    </form>
    <br>
    @include('messages.flash')
    <br>
</div>
@stop
@section('style')
<style type="text/css">
body {
    border-top: 4px solid #007bff;
}
#auth {
    width: 400px;
    margin: 20px auto;
}

select:required:invalid {
  color: gray;
}
option[value=""][disabled] {
  display: none;
}
</style>
@endsection