import fontawesome from '@fortawesome/fontawesome'
import faUser from '@fortawesome/fontawesome-free-solid/faUser'
import faHome from '@fortawesome/fontawesome-free-solid/faHome'
import faKey from '@fortawesome/fontawesome-free-solid/faKey'

fontawesome.library.add(faUser, faHome, faKey)