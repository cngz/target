window.$ = window.jQuery = require('jquery')
window.moment = require('moment')

require('bootstrap')

import swal from 'sweetalert'

$('[data-toggle="tooltip"]').tooltip()

window.onerror = function(e) {
    alert("An unknown error occurred, try to refresh the page - " + e)
}