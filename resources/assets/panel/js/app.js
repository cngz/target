window.$ = window.jQuery = require('jquery')
window.moment = require('moment')

require('bootstrap')

import JSZip from 'jszip'
import swal from 'sweetalert'

import 'devextreme/integration/jquery'
import 'devextreme/bundles/modules/ui'
import 'devextreme/bundles/modules/data'
import 'devextreme/ui/data_grid'
// import 'devextreme/ui/popup'
// import 'devextreme/ui/form'
import 'devextreme/ui/drop_down_box'
import 'devextreme/ui/switch'

import 'devextreme-intl';
// Dictionaries for Russian languages
import ruMessages from 'devextreme/localization/messages/ru.json';
import { locale, loadMessages } from 'devextreme/localization';

loadMessages(ruMessages);
locale('ru')


DevExpress.config({
    forceIsoDateParsing: true,
})

$('[data-toggle="tooltip"]').tooltip()

window.onerror = function(e) {
    alert("An unknown error occurred, try to refresh the page - " + e)
}