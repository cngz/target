import axios from 'axios'

export const HTTP = axios.create({
    baseURL: document.head.querySelector('meta[name="base-url"]'),
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]'),
    }
})
