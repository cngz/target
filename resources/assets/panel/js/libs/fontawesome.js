import fontawesome from '@fortawesome/fontawesome'
import faUserSecret from '@fortawesome/fontawesome-free-solid/faUserSecret'
import faUserAstronaut from '@fortawesome/fontawesome-free-solid/faUserAstronaut'
import faUserTie from '@fortawesome/fontawesome-free-solid/faUserTie'
import faFemale from '@fortawesome/fontawesome-free-solid/faFemale'
import faMale from '@fortawesome/fontawesome-free-solid/faMale'
import faHome from '@fortawesome/fontawesome-free-solid/faHome'
import faBus from '@fortawesome/fontawesome-free-solid/faBus'
import faCog from '@fortawesome/fontawesome-free-solid/faCog'
import faChild from '@fortawesome/fontawesome-free-solid/faChild'
import faCreditCard from '@fortawesome/fontawesome-free-solid/faCreditCard'
import faCalculator from '@fortawesome/fontawesome-free-solid/faCalculator'
import faMapMarkerAlt from '@fortawesome/fontawesome-free-solid/faMapMarkerAlt'
import faPowerOff from '@fortawesome/fontawesome-free-solid/faPowerOff'
import faAngleRight from '@fortawesome/fontawesome-free-solid/faAngleRight'
import faChevronLeft from '@fortawesome/fontawesome-free-solid/faChevronLeft'
import faCheckSquare from '@fortawesome/fontawesome-free-solid/faCheckSquare'
import farCheckSquare from '@fortawesome/fontawesome-free-regular/faCheckSquare'
import farSquare from '@fortawesome/fontawesome-free-regular/faSquare'
import farEdit from '@fortawesome/fontawesome-free-regular/faEdit'
import farTrashAlt from '@fortawesome/fontawesome-free-regular/faTrashAlt'

fontawesome.library.add(
    faUserSecret,
    faUserAstronaut,
    faUserTie,
    faFemale,
    faMale,
    faHome,
    faBus,
    faCog,
    faChild,
    faCreditCard,
    faCalculator,
    faMapMarkerAlt,
    faPowerOff,
    faAngleRight,
    faChevronLeft,
    faCheckSquare,
    farCheckSquare,
    farSquare,
    farEdit,
    farTrashAlt
  )