import {ru_RU} from './lang.js'

export const trans = function(str) {
    let locale = localStorage ? localStorage.getItem("vue-app-locale") || 'ru' : 'ru'

    return locale == 'ru' ? ru_RU[str] || str : str
}
