<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SearchTranslate extends Command
{
    /**
     * The name and signature of the console command.
     * artisan atrans:missing
     * @var string
     */
    protected $signature = 'atrans:missing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search texts in the app, compare with lang files, and translate it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = \File::allFiles(app_path('Console'));
        $this->comment(count($files));
        foreach ($files as $key => $file) {
            $this->comment($file);
        }
    }
}
