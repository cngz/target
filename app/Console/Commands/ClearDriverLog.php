<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class ClearDriverLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear drivers old log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('locations')->where('create_date', '<', Carbon::now()->subDay(30))->delete();
    }
}
