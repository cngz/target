<?php

function isprod() {
    return config('app.env') == 'production';
}

function assets($type, $file) {
    return '/assets/' . $type . '-' . (config('app.env') == 'production' ? 'prod' : 'dev') . '.' . $file . '?' . 1000007;
}

function get_letter($key) {
    $en = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $ru = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ш', 'Ы', 'Э', 'У'];

    return $en[min($key, count($en) - 1)];
}

function generate_uid($length = 100)
{
    $uid = uniqid();
    return strlen($uid) > $length ? $uid : $uid.strtolower(str_random($length - strlen($uid)));
}

function first_arr($arr, $key) {
    return @$arr[0][$key];
}

function last_arr($arr, $key) {
    return @$arr[count($arr) - 1][$key];
}

function week_names()
{
    return [
        1 => trans('site.Monday'),
        2 => trans('site.Tuesday'),
        3 => trans('site.Wednesday'),
        4 => trans('site.Thursday'),
        5 => trans('site.Friday'),
        6 => trans('site.Saturday'),
        0 => trans('site.Sunday'),
    ];
}

function week_name($day) {
    $day = @$day >= 0 && @$day <=6 ? $day : 0;

    $names = week_names();

    return $names[$day];
}

function month_names()
{
    return [
        1  => trans('site.January'),
        2  => trans('site.February'),
        3  => trans('site.March'),
        4  => trans('site.April'),
        5  => trans('site.May'),
        6  => trans('site.June'),
        7  => trans('site.July'),
        8  => trans('site.August'),
        9  => trans('site.September'),
        10 => trans('site.October'),
        11 => trans('site.November'),
        12 => trans('site.December'),
    ];
}

function month_name($month)
{
    $month = @$month >= 1 && @$month <=12 ? $month : 1;

    $names = month_names();

    return $names[$month];
}

function lname() {
    return app()->getlocale() == 'ru' ? 'name_ru' : 'name';
}

function getlocale($locale, $default = 'en') {
    return in_array($locale, config('app.locales')) ? $locale : $default;
}

function getlocales() {
    return config('app.locales');
}