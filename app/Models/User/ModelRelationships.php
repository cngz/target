<?php

namespace Models\User;

trait ModelRelationships
{
    public function messages() {
        return $this->hasMany(\Models\Message\Message::class, 'user_id');
    }
}
