<?php

namespace Models\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable, ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'users';
    protected $guarded      = ['id'];
    protected $hidden       = ['password', 'remember_token'];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }

    // /**
    //  * Send the password reset notification.
    //  *
    //  * @param  string  $token
    //  * @return void
    //  */
    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new ResetPasswordNotification($token));
    // }
}
