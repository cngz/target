<?php

namespace Models\User;

trait ModelHelpers
{
    public function isAdmin() {
        return in_array($this->role, ['admin']);
    }

    public function isNotAdmin() {
        return $this->isAdmin() == false;
    }

    public function isManager() {
        return in_array($this->role, ['manager']);
    }

    public function isNotManager() {
        return $this->isManager() == false;
    }

    public function isActive() {
        return $this->active == 1;
    }

    public function isNotActive() {
        return $this->active == 0;
    }
}
