<?php

namespace Models\Parent;

use Illuminate\Database\Eloquent\Model;

/* 2 t because 1 t gives error, maybe parent reserved word */
class Parentt extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'parents';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}
