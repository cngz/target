<?php

namespace Models\Parent;

trait ModelHelpers
{
    public function update_balance() {
        $payments = $this->payments()->whereBetween('date', [$this->start_date, $this->end_date])->get();

        $total = 0;
        foreach ($payments as $payment) {
            $total += $payment->amount;
        }

        $this->payments_total = $total;
        $this->balance = $total - $this->fare;

        $this->save();
    }

    public function hasDevice() {
        return in_array(strtolower($this->device_type), ['android', 'ios', 'win']) && strlen(trim($this->device_token)) > 10;
    }
}
