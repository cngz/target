<?php

namespace Models\Parent;

trait ModelRelationships
{
    public function children() {
        return $this->hasMany(\Models\Student\Student::class, 'parent_id');
    }

    public function payments() {
        return $this->hasMany(\Models\Payment\Payment::class, 'parent_id');
    }

    public function messages() {
        return $this->belongsToMany(\Models\Message\Message::class, 'parent_message', 'parent_id', 'message_id');
    }

    public function check_logs() {
        return $this->hasMany(\Models\CheckLog\CheckLog::class, 'parent_id');
    }
}
