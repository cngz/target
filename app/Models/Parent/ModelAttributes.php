<?php

namespace Models\Parent;

trait ModelAttributes
{
    public function getApiTimestampAttribute() {
        return strtotime($this->api_date) * 1000;
    }
}
