<?php

namespace Models\Student;

trait ModelRelationships
{
    public function school() {
        return $this->belongsTo(\Models\School\School::class, 'school_id');
    }

    public function parent() {
        return $this->belongsTo(\Models\Parent\Parentt::class, 'parent_id');
    }

    public function driver() {
        return $this->belongsTo(\Models\Driver\Driverr::class, 'driver_id');
    }

    public function actions() {
        return $this->hasMany(\Models\Action\Action::class, 'student_id')->orderBy('take_date', 'desc');
    }

    public function today_actions() {
        return $this->hasMany(\Models\Action\Action::class, 'student_id')
                    ->where('take_date', '>=', date('Y-m-d 00:00:00'))
                    ->where('take_date', '<=', date('Y-m-d 23:59:59'))
                    ->orderBy('id', 'desc');
    }


    public function check_logs() {
        return $this->hasMany(\Models\CheckLog\CheckLog::class, 'student_id');
    }
}
