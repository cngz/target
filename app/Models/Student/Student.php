<?php

namespace Models\Student;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'students';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}
