<?php

namespace Models\Student;

class ModelObserver
{

    public function creating($model)
    {
        if(empty($model->uuid)) {
            $model->uuid = \Uuid::uuid4()->toString();
        }
    }

    public function created($model)
    {

    }

    public function updating($model)
    {

    }

    public function updated($model)
    {

    }

    public function deleting($model)
    {

    }

    public function deleted($model)
    {

    }

    public function saving($model)
    {

    }

    public function saved($model)
    {

    }

    public function restoring($model)
    {

    }

    public function restored($model)
    {

    }

}
