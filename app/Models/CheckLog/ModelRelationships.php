<?php

namespace Models\CheckLog;

trait ModelRelationships
{
    public function student() {
        return $this->belongsTo(\Models\Student\Student::class, 'student_id');
    }

    public function parent() {
        return $this->belongsTo(\Models\Parent\Parentt::class, 'parent_id');
    }
}
