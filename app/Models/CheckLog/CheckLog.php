<?php

namespace Models\CheckLog;

use Illuminate\Database\Eloquent\Model;

class CheckLog extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'check_logs';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}