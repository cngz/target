<?php

namespace Models\Location;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table   = 'locations';
    protected $guarded = ['id'];
    protected $hidden  = [];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}
