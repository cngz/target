<?php

namespace Models\Location;

trait ModelScopes
{
    public function scopeActive($query) {
        return $query->where('active', true);
    }

    public function scopeRoles($query, ...$roles) {
        return $query->whereIn('role', $roles);
    }
}
