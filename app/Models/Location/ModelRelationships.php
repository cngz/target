<?php

namespace Models\Location;

trait ModelRelationships
{
    public function driver() {
        return $this->belongsTo(\Models\Driver\Driverr::class, 'driver_id');
    }
}