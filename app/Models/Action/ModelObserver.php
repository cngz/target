<?php

namespace Models\Action;

class ModelObserver
{

    public function creating($model)
    {

    }

    public function created($model)
    {

    }

    public function updating($model)
    {

    }

    public function updated($model)
    {

    }

    public function deleting($model)
    {

    }

    public function deleted($model)
    {

    }

    public function saving($model)
    {

    }

    public function saved($model)
    {

    }

    public function restoring($model)
    {

    }

    public function restored($model)
    {

    }

}
