<?php

namespace Models\Action;

trait ModelRelationships
{
    public function student() {
        return $this->belongsTo(\Models\Student\Student::class, 'student_id');
    }

    public function driver() {
        return $this->belongsTo(\Models\Driver\Driverr::class, 'driver_id');
    }
}
