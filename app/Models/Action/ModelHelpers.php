<?php

namespace Models\Action;

trait ModelHelpers
{
    public function notify_parent($action, $status) {
        if(@$this->student->parent->hasDevice()) {
            if($this->type == 'h2s') {
                if($action == 'take' && $status == 1) {
                    $data['push_title']   = @$this->student->name;
                    $data['push_message'] = \Carbon\Carbon::parse($this->take_date)->format('H:i, d-m-Y') . ', сел в автобус';;
                } else if($action == 'take' && $status == 0) {
                    $data['push_title']   = @$this->student->name;
                    $data['push_message'] = \Carbon\Carbon::parse($this->take_date)->format('H:i, d-m-Y') . ', не пришел';
                } else if($action == 'leave') {
                    $data['push_title']   = @$this->student->name;
                    $data['push_message'] = \Carbon\Carbon::parse($this->leave_date)->format('H:i, d-m-Y') . ', в школе';
                }
            } elseif($this->type == 's2h') {
                if($action == 'take' && $status == 1) {
                    $data['push_title']   = @$this->student->name;
                    $data['push_message'] = \Carbon\Carbon::parse($this->take_date)->format('H:i, d-m-Y') . ', сел в автобус';
                } else if($action == 'take' && $status == 0) {
                    $data['push_title']   = @$this->student->name;
                    $data['push_message'] = \Carbon\Carbon::parse($this->take_date)->format('H:i, d-m-Y') . ', не пришел';
                } else if($action == 'leave') {
                    $data['push_title']   = @$this->student->name;
                    $data['push_message'] = \Carbon\Carbon::parse($this->leave_date)->format('H:i, d-m-Y') . ', дома';
                }
            }

            $data['push_type']    = 'action';
            $data['device_type']  = @$this->student->parent->device_type;
            $data['device_token'] = @$this->student->parent->device_token;

            $msg = \PushNotification::Message($data['push_title'], [
                'badge' => 1,
                'sound' => 'default',
                'custom' => [
                    'type'    => $data['push_type'],
                    'message' => $data['push_message'],
                ]
            ]);

            \PushNotification::app($data['device_type'])
                                ->to($data['device_token'])
                                ->send($msg);
        }
    }
}
