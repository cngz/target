<?php

namespace Models\School;

trait ModelScopes
{
    public function scopeActive($query) {
        return $query->where('active', true);
    }
}
