<?php

namespace Models\School;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'schools';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}