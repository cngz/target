<?php

namespace Models\School;

trait ModelRelationships
{
    public function drivers() {
        return $this->hasMany(\Models\Driver\Driverr::class, 'school_id');
    }

    public function students() {
        return $this->hasMany(\Models\Student\Student::class, 'school_id');
    }
}
