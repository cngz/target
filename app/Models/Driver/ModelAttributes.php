<?php

namespace Models\Driver;

trait ModelAttributes
{
    public function getApiTimestampAttribute() {
        return strtotime($this->api_date) * 1000;
    }
}
