<?php

namespace Models\Driver;

use Illuminate\Database\Eloquent\Model;

/* 2 r because 1 t gives error, maybe driver reserved word */
class Driverr extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'drivers';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}
