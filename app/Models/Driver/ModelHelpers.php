<?php

namespace Models\Driver;

trait ModelHelpers
{
    public function update_location($lat, $lng) {
        $this->update([
            'lat' => $lat,
            'lng' => $lng,
        ]);

        $this->locations()->create([
            'lat' => $lat,
            'lng' => $lng,
        ]);
    }

    public function hasDevice() {
        return in_array(strtolower($this->device_type), ['android', 'ios', 'win']) && strlen(trim($this->device_token)) > 10;
    }
}
