<?php

namespace Models\Driver;

trait ModelRelationships
{
    public function school() {
        return $this->belongsTo(\Models\School\School::class, 'school_id');
    }

    public function passengers() {
        return $this->hasMany(\Models\Student\Student::class, 'driver_id')->orderBy('order', 'asc')->orderBy('id', 'asc');
    }

    public function locations() {
        return $this->hasMany(\Models\Location\Location::class, 'driver_id');
    }

    public function actions() {
        return $this->hasMany(\Models\Action\Action::class, 'driver_id')->orderBy('take_date', 'desc');
    }

    public function messages() {
        return $this->belongsToMany(\Models\Message\Message::class, 'driver_message', 'driver_id', 'message_id');
    }
}
