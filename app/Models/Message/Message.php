<?php

namespace Models\Message;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'messages';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}
