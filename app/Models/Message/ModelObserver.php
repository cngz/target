<?php

namespace Models\Message;

class ModelObserver
{

    public function creating($model)
    {
        $model->create_date = now()->format('Y-m-d H:i:s');
    }

    public function created($model)
    {

    }

    public function updating($model)
    {

    }

    public function updated($model)
    {

    }

    public function deleting($model)
    {

    }

    public function deleted($model)
    {

    }

    public function saving($model)
    {

    }

    public function saved($model)
    {

    }

    public function restoring($model)
    {

    }

    public function restored($model)
    {

    }

}
