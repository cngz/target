<?php

namespace Models\Message;

trait ModelAttributes
{
    public function getTitleAttribute($value) {
        return (string) $value;
    }

    public function getContentAttribute($value) {
        return (string) $value;
    }
}
