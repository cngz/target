<?php

namespace Models\Message;

trait ModelRelationships
{
    public function sender() {
        return $this->belongsTo(\Models\User\User::class, 'user_id');
    }

    public function parents()
    {
        return $this->belongsToMany(\Models\Parent\Parentt::class, 'parent_message', 'message_id', 'parent_id');
    }

    public function drivers()
    {
        return $this->belongsToMany(\Models\Driver\Driverr::class, 'driver_message', 'message_id', 'driver_id');
    }
}
