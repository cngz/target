<?php

namespace Models\Payment;

trait ModelRelationships
{
    public function parent() {
        return $this->belongsTo(\Models\Parent\Parentt::class, 'parent_id');
    }
}
