<?php

namespace Models\Payment;

trait ModelAttributes
{
    public function getDescAttribute($value) {
        return (string) $value;
    }
}
