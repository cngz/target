<?php

namespace Models\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'payments';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}