<?php

namespace Models\Payment;

class ModelObserver
{

    public function creating($model)
    {
        $model->create_date = now()->format('Y-m-d H:i:s');
    }

    public function created($model)
    {

    }

    public function updating($model)
    {

    }

    public function updated($model)
    {

    }

    public function deleting($model)
    {

    }

    public function deleted($model)
    {
        $model->parent->update_balance();
    }

    public function saving($model)
    {

    }

    public function saved($model)
    {
        $model->parent->update_balance();
    }

    public function restoring($model)
    {

    }

    public function restored($model)
    {

    }

}
