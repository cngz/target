<?php

namespace Models\News;

class ModelObserver
{

    public function creating($model)
    {
        if(empty($model->uuid)) {
            $model->uuid = \Uuid::uuid4()->toString();
        }

        $model->create_date = now()->format('Y-m-d H:i:s');
    }

    public function created($model)
    {

    }

    public function updating($model)
    {

    }

    public function updated($model)
    {

    }

    public function deleting($model)
    {

    }

    public function deleted($model)
    {

    }

    public function saving($model)
    {

    }

    public function saved($model)
    {

    }

    public function restoring($model)
    {

    }

    public function restored($model)
    {

    }

}
