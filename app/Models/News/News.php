<?php

namespace Models\News;

use Illuminate\Database\Eloquent\Model;

/* 2 r because 1 t gives error, maybe News reserved word */
class News extends Model
{
    use ModelAttributes, ModelHelpers, ModelRelationships, ModelScopes;

    protected $table        = 'news';
    protected $guarded      = ['id'];
    protected $hidden       = [];
    public    $timestamps   = false;

    public static function boot()
    {
        parent::boot();
        self::observe(new ModelObserver);
    }
}
