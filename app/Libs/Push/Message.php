<?php

namespace Libs\Push;

use Models\User\User;
use Models\Article\Article;
use Models\Message\Message as MessageModel;

class PushMessage
{
    public static function send($model, $users = []) {
        $result = [];
        $fields = [];

        if($model instanceof Article) {
            $fields = self::prepareArticleFields($model, $user, $message, $users);
        } else if($model instanceof Report) {
            $fields = self::prepareReportFields($model, $user, $message, $users);
        } else if($model instanceof MessageModel) {
            $fields = self::prepareMessageFields($model, $user, $message, $users);
        }

        if(!empty($fields)) {
            $headers = ['Authorization: key='.env('FCM_API_KEY'), 'Content-Type: application/json'];

            //Send Reponse To FireBase Server
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
        }

        return $result;
    }

    public static function prepareArticleFields($model, User $user = null, $message = null, $users = []) {
        $fields = [];

        $tokens = User::whereNotNull('mobile_token')->pluck('mobile_token')->toArray();

        if(count($tokens) > 0)
        {
            //prep the bundle
            $fields = [
                'registration_ids' => $tokens,
                'notification'  => [
                    'title' => 'Новость',
                    'body'  => $message ?: $model->name,
                    "sound" => "default",
                ]
            ];

        }

        return $fields;
    }

    public static function prepareReportFields($model, User $user = null, $message = null, $users = []) {
        $fields = [];
        $tokens = [];

        if($user && count($user) > 0)
        {
            $tokens[] = $user->mobile_token;

            //prep the bundle
            $fields = [
                'registration_ids' => $tokens,
                'notification'  => [
                    'title' => 'Заявки',
                    'body'  => empty($message) ? $model->desc : $message,
                    "sound" => "default",
                ]
            ];

        }

        return $fields;
    }

    public static function prepareMessageFields($model, User $user = null, $message = null, $users = []) {
        $fields = [];

        if(count($users) > 0)
        {
            $tokens = User::whereIn('id', $users)->whereNotNull('mobile_token')->pluck('mobile_token')->toArray();

            //prep the bundle
            $fields = [
                'registration_ids' => $tokens,
                'notification'  => [
                    'title' => 'Сообщение',
                    'body'  => $message ?: $model->desc,
                    "sound" => "default",
                ]
            ];
        }

        return $fields;
    }
}
