<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;

trait Message
{
    public function message_list() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $messages = $parent->messages()
                            ->where(function($query) {
                                if(request()->has('start_date')) {
                                    $query->where('create_date', '>=', request('start_date') . ' 00:00:00');
                                }
                                if(request()->has('end_date')) {
                                    $query->where('create_date', '<=', request('end_date') . ' 23:59:59');
                                }
                            })
                            ->select('title', 'content', 'create_date')
                            ->orderBy('message_id', 'desc')
                            ->get();

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = $messages;

        return $this->response;
    }
}
