<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;

trait Payment
{
    public function payment_list() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $payments = $parent->payments()
                            ->where(function($query) {
                                if(request()->has('start_date')) {
                                    $query->where('date', '>=', request('start_date') . ' 00:00:00');
                                }
                                if(request()->has('end_date')) {
                                    $query->where('date', '<=', request('end_date') . ' 23:59:59');
                                }
                            })
                            ->select('amount', 'date', 'desc', 'create_date')
                            ->orderBy('date', 'desc')
                            // ->skip(0)
                            // ->take(50)
                            ->get();

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = $payments;

        return $this->response;
    }
}
