<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;
use Models\Action\Action;

trait ActionTrait
{
    public function action_list() {
        $params = $this->prepare(['api_token', 'student']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $actions = Action::where('student_id', $params['student'])
                            ->where(function($query) {
                                if(request()->has('driver')) {
                                    $query->where('driver_id', request('driver'));
                                }
                                if(request()->has('start_date')) {
                                    $query->where('take_date', '>=', request('start_date') . ' 00:00:00');
                                }
                                if(request()->has('end_date')) {
                                    $query->where('take_date', '<=', request('end_date') . ' 23:59:59');
                                }
                            })
                            ->select(\DB::raw('type, take_date, leave_date, cast(take_date as date) `date`, status'))
                            ->orderBy('date', 'desc')
                            ->get();

        $data = [];

        foreach($actions as $action) {

            $tmp['date']       = \Carbon\Carbon::parse($action->date)->format('d.m.Y');

            $tmp['h2s_take']   = "";
            $tmp['h2s_leave']  = "";

            $tmp['s2h_take']   = "";
            $tmp['s2h_leave']  = "";

            $h2sact = $actions->where('type', 'h2s')->where('date', $action->date)->where('status', true)->first();
            if($h2sact) {
                if($h2sact->take_date)
                    $tmp['h2s_take']   = \Carbon\Carbon::parse($h2sact->take_date)->format('H:i');

                if($h2sact->leave_date)
                    $tmp['h2s_leave']  = \Carbon\Carbon::parse($h2sact->leave_date)->format('H:i');
            }

            $s2hact = $actions->where('type', 's2h')->where('date', $action->date)->where('status', true)->first();
            if($s2hact) {
                if($s2hact->take_date)
                    $tmp['s2h_take']   = \Carbon\Carbon::parse($s2hact->take_date)->format('H:i');
                if($s2hact->leave_date)
                    $tmp['s2h_leave']  = \Carbon\Carbon::parse($s2hact->leave_date)->format('H:i');
            }

            $add = true;
            foreach ($data as $dat) {
                if($dat['date'] == $tmp['date']) {
                    $add = false;
                }
            }

            if($add)
                $data[] = $tmp;
        }

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = $data;

        return $this->response;
    }
}
