<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;

trait Auth
{
    public function login() {
        /* check for required params */
        $params = $this->prepare(['phone', 'password', 'device_type', 'device_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(in_array($params['device_type'], ['android', 'ios', 'win']) == false)
            return Error::response('incorrect_device_type');

        $parent = Parentt::where('phone', $params['phone'])->first();

        if(empty($parent) || \Hash::check($params['password'], @$parent->password) == false)
            return Error::response('incorrect_login');

        /* if everything is ok */
        $parent->device_type  = strtolower($params['device_type']);
        $parent->device_token = $params['device_token'];

        try {
            if(empty($parent->api_token)) {
                $parent->api_token = md5($parent->id) . str_random(68); /* length 100 */
                $parent->api_date  = now()->addMonths(12)->format('Y-m-d H:i:s');
            } else {
                $date = \Carbon\Carbon::parse($parent->api_date);

                if($date < now()->addSeconds(5)) {
                    $parent->api_token = md5($parent->id) . str_random(68); /* length 100 */
                    $parent->api_date  = now()->addMonths(12)->format('Y-m-d H:i:s');
                }
            }

        } catch (\Exception $e) {

        }

        $parent->save();


        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = [
            'name'      => (string) $parent->name,
            'phone'     => (string) $parent->phone,
            'balance'   => (float) $parent->balance,
            'address'   => (string) $parent->address,
            'api_token' => (string) $parent->api_token,
            'api_date'  => (int) $parent->api_timestamp,
            'desc'      => (string) $parent->desc,
        ];

        return $this->response;
    }

    public function logout() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* phone number may start with 0 */
        $parent = Parentt::where('api_token', $params["api_token"])->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $parent->update([
            'api_token' => null,
            'api_date'  => null,
        ]);

        $this->response['status']  = 0;
        $this->response['message'] = trans('site.Parent is logged out');

        return $this->response;
    }
}
