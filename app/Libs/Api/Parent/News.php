<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;

trait News
{
    public function news_list() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $news = \Models\News\News::select('title', 'content', 'create_date','image')
            ->orderBy('create_date', 'desc')
            ->get();

        $this->response['status']  = 0;
        $this->response['news'] = '';
        $this->response['data']    = $news;

        return $this->response;
    }
}
