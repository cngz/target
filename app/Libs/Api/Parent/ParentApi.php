<?php

namespace Libs\Api\Parent;

class ParentApi extends \Libs\Api\BaseApi
{
    use Auth, Child, Location, Payment, Profile, Message, ActionTrait, News, Banner;

    public function __construct() {
        parent::__construct();
    }
}
