<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;
use Models\Driver\Driverr;

trait Location
{
    public function driver_location() {
        $params = $this->prepare(['api_token', 'driver']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $driver = Driverr::find($params['driver']);

        if(empty($driver))
            return Error::response('item_not_found');

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = [
            'lat'  => (float) $driver->lat,
            'lng'  => (float) $driver->lng,
        ];

        return $this->response;
    }
}
