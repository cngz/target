<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;

trait Child
{
    public function child_list() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        $children = $parent->children()->with('driver', 'actions')->get();

        $data = [];

        foreach ($children as $key => $child) {
            $data[$key]['id']        = (int)    $child->id;
            $data[$key]['name']      = (string) $child->name;
            $data[$key]['phone']     = (string) $child->phone;
            $data[$key]['image']     = (string) $child->image;
            $data[$key]['signature'] = (string) strtotime($child->signature);
            $data[$key]['desc']      = (string) $child->desc;
            $data[$key]['h2s']       = $child->h2s_date == date('Y-m-d');
            $data[$key]['s2h']       = $child->s2h_date == date('Y-m-d');

            $data[$key]['driver']['id']      = data_get($child, 'driver.id');
            $data[$key]['driver']['name']    = data_get($child, 'driver.name', 'Нет водителя');
            $data[$key]['driver']['carno']   = data_get($child, 'driver.carno', '');
            $data[$key]['driver']['carname'] = data_get($child, 'driver.carname', '');
            $data[$key]['driver']['phone']   = data_get($child, 'driver.phone', '');
            $data[$key]['driver']['desc']    = data_get($child, 'driver.desc', '');
            $data[$key]['driver']['lat']     = (float)  data_get($child, 'driver.lat', 0);
            $data[$key]['driver']['lng']     = (float)  data_get($child, 'driver.lng', 0);

            $actions['date']       = date('d.m.Y');
            $actions['h2s_take']   = "";
            $actions['h2s_leave']  = "";
            $actions['s2h_take']   = "";
            $actions['s2h_leave']  = "";
            $actions['h2s_status'] = -1;
            $actions['s2h_status'] = -1;

            foreach($child->today_actions as $action) {
                // if($action->status == true) {
                    if($action->type == 'h2s') {
                        if(empty($action->take_date) == false)
                            $actions['h2s_take']  = \Carbon\Carbon::parse($action->take_date)->format('H:i');
                        if(empty($action->leave_date) == false)
                            $actions['h2s_leave'] = \Carbon\Carbon::parse($action->leave_date)->format('H:i');

                        $actions['h2s_status'] = $action->status ? 1 : -1;
                    }
                    if($action->type == 's2h') {
                        if(empty($action->take_date) == false)
                            $actions['s2h_take']  = \Carbon\Carbon::parse($action->take_date)->format('H:i');
                        if(empty($action->leave_date) == false)
                            $actions['s2h_leave'] = \Carbon\Carbon::parse($action->leave_date)->format('H:i');

                        $actions['s2h_status'] = $action->status ? 1 : -1;
                    }
                // }
            }

            $data[$key]['action'] = $actions;
        }

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = $data;

        return $this->response;
    }


    public function child_upload() {
        $params = $this->prepare(['api_token', 'child']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(request()->hasFile('image') == false)
            return Error::response('parameter_required');

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        // $child = $parent->children;
        $child = $parent->children()->where('id', $params['child'])->first();

        if(empty($child))
            return Error::response('item_not_found');

        $file = request()->file('image');
        $dir  = 'img/upload';
        $root = '/storage';

        \Storage::disk('public')->makeDirectory($dir);

        $ext = $file->extension();

        if(!in_array($file->extension(), ['png', 'jpg', 'jpeg']))
            return Error::response('wrong_image_type');

        /* max 4 mb */
        if($file->getClientSize() >= 4 * 1024 * 1024)
            return Error::response('file_size_too_big');

        $path  = $dir . '/' . $child->uuid . '.jpg';

        $img = \Intervention::make($file)
                    ->interlace()
                    ->fit(800, 800)
                    ->encode('jpg', 80)
                    ->save(storage_path('app/public/' . $path));

        $img->destroy();

        $child->update([
            'image'     => asset('storage/' . $path),
            'signature' => now(),
        ]);

        $this->response['status']  = 0;
        $this->response['message'] = trans('site.Image was uploaded successfully');
        $this->response['data']    = $child;

        return $this->response;
    }


    public function child_action() {
        $params = $this->prepare(['api_token', 'child', 'type', 'take']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(in_array($params['type'], ['h2s', 's2h']) == false || in_array($params['take'], ['true', 'false', true, false, 0, 1]) == false)
            return Error::response('wrong_variable_is_received');

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($parent))
            return Error::response('token_time_expired');

        // $child = $parent->children;
        $child = $parent->children()->where('id', $params['child'])->first();

        if(empty($child))
            return Error::response('item_not_found');

        if($params['type'] == 'h2s')
            $child->update(['h2s_date' => $params['take'] == 'true' ? date('Y-m-d') : null ]);

        if($params['type'] == 's2h')
            $child->update(['s2h_date' => $params['take'] == 'true' ? date('Y-m-d') : null ]);

        $parent->check_logs()->create([
            'student_id' => $child->id,
            'type'       => $params['type'],
            'checked'    => $params['take'] == 'true' ? 2 : 1,
        ]);

        $this->response['status']  = 0;
        $this->response['message'] = trans('site.Your request was saved');
        $this->response['data']    = $child;

        return $this->response;
    }
}
