<?php

namespace Libs\Api\Parent;

use Libs\Api\Error;
use Models\Parent\Parentt;

trait Banner
{
    public function banner_get()
    {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if ($this->response['status'] > 0)
            return $this->response;

        /* check parent api token and valid date */
        $parent = Parentt::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if (empty($parent))
            return Error::response('token_time_expired');

        $images = \Storage::disk('public')->files('img/banner');
        rsort($images);

        if (count($images)) {
            foreach ($images as $key=> $image)
            {
                $images[$key]  =array(
                    'image'=>env('APP_URL') . "/storage/" . $image,
                );
            }
        } else {
            $images = null;
        }

        $this->response['status'] = 0;
        $this->response['banner'] = '';
        $this->response['data'] = $images;

        return $this->response;
    }
}
