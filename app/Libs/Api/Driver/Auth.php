<?php

namespace Libs\Api\Driver;

use Libs\Api\Error;
use Models\Driver\Driverr;

trait Auth
{
    public function login() {
        /* check for required params */
        $params = $this->prepare(['phone', 'password', 'device_type', 'device_token', 'lat', 'lng']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(in_array($params['device_type'], ['android', 'ios', 'win']) == false)
            return Error::response('incorrect_device_type');

        $driver = Driverr::where('phone', $params['phone'])->first();

        if(empty($driver) || \Hash::check($params['password'], @$driver->password) == false)
            return Error::response('incorrect_login');

        /* if everything is ok */
        $driver->device_type  = strtolower($params['device_type']);
        $driver->device_token = $params['device_token'];
        $driver->lat          = $params['lat'];
        $driver->lng          = $params['lng'];

        try {
            if(empty($driver->api_token)) {
                $driver->api_token = md5($driver->id) . str_random(68); /* length 100 */
                $driver->api_date  = now()->addMonths(12)->format('Y-m-d H:i:s');
            } else {
                $date = \Carbon\Carbon::parse($driver->api_date);

                if($date < now()->addSeconds(5)) {
                    $driver->api_token = md5($driver->id) . str_random(68); /* length 100 */
                    $driver->api_date  = now()->addMonths(12)->format('Y-m-d H:i:s');
                }
            }

        } catch (\Exception $e) {

        }

        $driver->save();

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = [
            'name'      => (string) $driver->name,
            'carno'     => (string) $driver->carno,
            'carname'   => (string) $driver->carname,
            'phone'     => (string) $driver->phone,
            'api_token' => (string) $driver->api_token,
            'api_date'  => (int) $driver->api_timestamp,
            'lat'       => (float) $driver->lat,
            'lng'       => (float) $driver->lng,
            'desc'      => (string) $driver->desc,
        ];

        return $this->response;
    }

    public function logout() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* phone number may start with 0 */
        $driver = Driverr::where('api_token', $params["api_token"])->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        $driver->update([
            'api_token' => null,
            'api_date'  => null,
        ]);

        $this->response['status']  = 0;
        $this->response['message'] = trans('site.Driver is logged out');

        return $this->response;
    }
}
