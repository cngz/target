<?php

namespace Libs\Api\Driver;

use Libs\Api\Error;
use Models\Driver\Driverr;
use Models\Action\Action;

trait Passenger
{
    public function passenger_list() {
        $params = $this->prepare(['api_token', 'type', 'action']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(in_array($params['type'], ['h2s', 's2h']) == false || in_array($params['action'], ['take', 'leave']) == false )
            return Error::response('wrong_variable_is_received');

        /* check driver api token and valid date */
        $driver = Driverr::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        $actions = $driver->actions()
                          ->where('type', $params['type'])
                          ->where(function($query) use ($params) {
                                $query->whereBetween('take_date', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')]);

                                if($params['action'] == 'leave') {
                                    $query->where('status', true);
                                    $query->whereNull('leave_date');
                                }
                          })
                          ->pluck('student_id')
                          ->toArray();

        $passengers = $driver->passengers()
                             ->with('parent')
                             ->where(function($query) use ($params, $actions) {
                                if($params['action'] == 'take') {
                                    $query->whereNotIn('id', $actions);
                                } else {
                                    $query->whereIn('id', $actions);
                                }
                             })
                             ->where(function($query) use ($params) {
                                if($params['type'] == 'h2s') {
                                    $query->whereNull('h2s_date');
                                    $query->orWhere('h2s_date', '<>', date('Y-m-d'));
                                } else if($params['type'] == 's2h') {
                                    $query->whereNull('s2h_date');
                                    $query->orWhere('s2h_date', '<>', date('Y-m-d'));
                                }
                             })
                             ->get();

        $data = [];

        foreach ($passengers as $key => $passenger) {
            $data[$key]['uuid']      = (string) $passenger->uuid;
            $data[$key]['name']      = (string) $passenger->name;
            $data[$key]['phone']     = (string) $passenger->phone;
            $data[$key]['image']     = (string) $passenger->image;
            $data[$key]['signature'] = (string) strtotime($passenger->signature);
            $data[$key]['desc']      = (string) $passenger->desc;
            $data[$key]['h2s']       = $passenger->h2s_date == date('Y-m-d');
            $data[$key]['s2h']       = $passenger->s2h_date == date('Y-m-d');

            $data[$key]['parent'] = [];

            if($passenger->parent) {
                $data[$key]['parent']['name']    = (string) $passenger->parent->name;
                $data[$key]['parent']['phone']   = (string) $passenger->parent->phone;
                $data[$key]['parent']['address'] = (string) $passenger->parent->address;
                $data[$key]['parent']['desc']    = (string) $passenger->parent->desc;
            }
        }

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = $data;

        return $this->response;
    }

    public function passenger_action() {
        $params = $this->prepare(['api_token', 'passenger', 'type', 'action', 'status', 'lat', 'lng']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(in_array($params['type'], ['h2s', 's2h']) == false ||
           in_array($params['action'], ['take', 'leave']) == false ||
           in_array($params['status'], [0, 1]) == false)
            return Error::response('wrong_variable_is_received');

        /* check driver api token and valid date */
        $driver = Driverr::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        $driver->update_location($params['lat'], $params['lng']);

        $student = $driver->passengers()->where('uuid', $params['passenger'])->first();

        if(empty($student))
            return Error::response('item_not_found');

        $action = Action::whereBetween('take_date', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])
                        ->firstOrCreate([
                            'driver_id'  => $driver->id,
                            'student_id' => $student->id,
                            'type'       => $params['type'],
                        ]);

        if($params['action'] == 'take') {
            $action->take_date = now();
            $action->status    = $params['status'];
        } else {
            $action->leave_date = now();
        }


        $action->save();

        $action->notify_parent($params['action'], $params['status']);

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = $student->today_actions;

        return $this->response;
    }


    public function passenger_reset() {
        $params = $this->prepare(['api_token']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check driver api token and valid date */
        $driver = Driverr::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        $driver->actions()
               ->whereBetween('take_date', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])
               ->delete();

        $this->response['status']  = 0;
        $this->response['message'] = trans('site.List was reset');
        $this->response['data']    = [];

        return $this->response;
    }


    public function passenger_order() {
        $params = $this->prepare(['api_token', 'uuids']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check driver api token and valid date */
        $driver = Driverr::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        foreach ($params['uuids'] as $key => $uuid) {
            $driver->passengers()->where('uuid', '=', $uuid)->update(['order' => $key]);
        }

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = [];

        return $this->response;
    }
}
