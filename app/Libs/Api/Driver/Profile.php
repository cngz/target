<?php

namespace Libs\Api\Driver;

use Libs\Api\Error;
use Models\Driver\Driverr;

trait Profile
{
    public function update_password() {
        $params = $this->prepare(['api_token', 'old_password', 'new_password']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        if(strlen($params['new_password']) < 6)
            return Error::response('text_is_too_short');

        /* check driver api token and valid date */
        $driver = Driverr::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        if(\Hash::check($params['old_password'], $driver->password) == false)
            return Error::response('incorrect_old_password');

        $driver->update(['password' => bcrypt($params['new_password']), 'api_date' => now()->subDays(1)]);

        $this->response['status']  = 0;
        $this->response['message'] = trans('site.Password was changed');
        $this->response['data']    = [];

        return $this->response;
    }
}
