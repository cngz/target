<?php

namespace Libs\Api\Driver;

class DriverApi extends \Libs\Api\BaseApi
{
    use Auth, Passenger, Location, Profile, Message;

    public function __construct() {
        parent::__construct();
    }
}
