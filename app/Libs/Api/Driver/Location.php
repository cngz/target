<?php

namespace Libs\Api\Driver;

use Libs\Api\Error;
use Models\Driver\Driverr;

trait Location
{
    public function update_location() {
        $params = $this->prepare(['api_token', 'lat', 'lng']);

        /* check for response error */
        if($this->response['status'] > 0)
            return $this->response;

        /* check driver api token and valid date */
        $driver = Driverr::where('api_token', $params["api_token"])->where('api_date', '>=', date('Y-m-d H:i:s'))->first();

        if(empty($driver))
            return Error::response('token_time_expired');

        $driver->update_location($params['lat'], $params['lng']);

        $this->response['status']  = 0;
        $this->response['message'] = '';
        $this->response['data']    = [
            'lat'  => (float) $driver->lat,
            'lng'  => (float) $driver->lng,
        ];

        return $this->response;
    }
}
