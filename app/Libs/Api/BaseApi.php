<?php

namespace Libs\Api;

class BaseApi
{
    protected $response;

    public function __construct() {
        $this->response = [
            'status'  => 0,
            'message' => '',
            'data'    => [],
        ];
    }

    protected function prepare($keys) {
        $params = request()->only($keys);

        if(count($keys) != count($params)) {
            $this->response = Error::response('parameter_required');
        } else {
            foreach($params as $param) {
                if($param == 0) {
                    continue;
                }

                if(empty($param)) {
                    $this->response = Error::response('parameter_empty');
                    break;
                }
            }
        }

        return $params;
    }
}
