<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        /* project views */
        $this->app['view']->addNamespace('Admin', app_path('Http/Admin/Views'));
        $this->app['view']->addNamespace('Front', app_path('Http/Front/Views'));
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapFrontRoutes();

        $this->mapAdminRoutes();
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
             ->middleware('admin')
             ->namespace($this->namespace.'\\Admin\\Controllers')
             ->group(app_path('Http/Admin/routes.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapFrontRoutes()
    {
        Route::middleware('front')
             ->namespace($this->namespace.'\\Front\\Controllers')
             ->group(app_path('Http/Front/routes.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group(['prefix' => 'api', 'namespace' => $this->namespace.'\\Api'], function()
        {
            /* API Version 1 */
            Route::prefix('v1')
                 ->middleware('api.v1')
                 ->namespace('v1\\Controllers')
                 ->group(app_path('Http/Api/v1/routes.php'));

            /* API Version 2 */
            Route::prefix('v2')
                 ->middleware('api.v2')
                 ->namespace('v2\\Controllers')
                 ->group(app_path('Http/Api/v2/routes.php'));

        });
    }
}
