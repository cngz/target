<?php

namespace App\Http\Api\v1\Controllers;

use Illuminate\Http\Request;
use Libs\Api\Parent\ParentApi;

class ParentController extends Controller
{
    protected $api;

    public function __construct(ParentApi $api)
    {
        $this->api = $api;
    }

    public function login() {
        $response = $this->api->login();

        return response()->json($response, 200);
    }

    public function logout() {
        $response = $this->api->logout();

        return response()->json($response, 200);
    }

    public function payment_list() {
        $response = $this->api->payment_list();

        return response()->json($response, 200);
    }

    public function banner_get() {
        $response = $this->api->banner_get();

        return response()->json($response, 200);
    }

    public function message_list() {
        $response = $this->api->message_list();

        return response()->json($response, 200);
    }

    public function news_list() {
        $response = $this->api->news_list();
        return response()->json($response, 200);
    }

    public function child_list() {
        $response = $this->api->child_list();

        return response()->json($response, 200);
    }

    public function child_upload() {
        $response = $this->api->child_upload();

        return response()->json($response, 200);
    }

    public function child_action() {
        $response = $this->api->child_action();

        return response()->json($response, 200);
    }

    public function action_list() {
        $response = $this->api->action_list();

        return response()->json($response, 200);
    }

    public function driver_location() {
        $response = $this->api->driver_location();

        return response()->json($response, 200);
    }

    public function update_password() {
        $response = $this->api->update_password();

        return response()->json($response, 200);
    }
}
