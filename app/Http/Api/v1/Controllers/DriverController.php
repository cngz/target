<?php

namespace App\Http\Api\v1\Controllers;

use Illuminate\Http\Request;
use Libs\Api\Driver\DriverApi;

class DriverController extends Controller
{
    protected $api;

    public function __construct(DriverApi $api)
    {
        $this->api = $api;
    }

    public function login() {
        $response = $this->api->login();

        return response()->json($response, 200);
    }

    public function logout() {
        $response = $this->api->logout();

        return response()->json($response, 200);
    }

    public function message_list() {
        $response = $this->api->message_list();

        return response()->json($response, 200);
    }

    public function passenger_list() {
        $response = $this->api->passenger_list();

        return response()->json($response, 200);
    }

    public function passenger_action() {
        $response = $this->api->passenger_action();

        return response()->json($response, 200);
    }

    public function passenger_reset() {
        $response = $this->api->passenger_reset();

        return response()->json($response, 200);
    }

    public function passenger_order() {
        $response = $this->api->passenger_order();

        return response()->json($response, 200);
    }

    public function update_location() {
        $response = $this->api->update_location();

        return response()->json($response, 200);
    }

    public function update_password() {
        $response = $this->api->update_password();

        return response()->json($response, 200);
    }
}
