<?php

Route::get('/', 'ApiController@index');

/* mobile driver routes ----------------------------------------------- */
Route::post('driver/login',  'DriverController@login');
Route::post('driver/logout', 'DriverController@logout');

Route::post('driver/message/list',    'DriverController@message_list');

Route::post('driver/passenger/list',  'DriverController@passenger_list');
Route::post('driver/passenger/action','DriverController@passenger_action');
Route::post('driver/passenger/reset', 'DriverController@passenger_reset');
Route::post('driver/passenger/order', 'DriverController@passenger_order');

Route::post('driver/update/location', 'DriverController@update_location');
Route::post('driver/update/password', 'DriverController@update_password');
/* --------------------------------------------------------------------- */

/* mobile parent routes ----------------------------------------------- */
Route::post('parent/login',  'ParentController@login');
Route::post('parent/logout', 'ParentController@logout');

Route::post('parent/payment/list',      'ParentController@payment_list');
Route::post('parent/message/list',      'ParentController@message_list');
Route::post('parent/banner/get',      'ParentController@banner_get');
Route::post('parent/news/list',         'ParentController@news_list');
Route::post('parent/child/list',        'ParentController@child_list');
Route::post('parent/child/upload',      'ParentController@child_upload');
Route::post('parent/child/action',      'ParentController@child_action');
Route::post('parent/action/list',       'ParentController@action_list');

Route::post('parent/driver/location', 'ParentController@driver_location');

Route::post('parent/update/password', 'ParentController@update_password');
/* --------------------------------------------------------------------- */

