<?php

Route::group(['middleware' => 'access:admin,manager'], function() {
    Route::get ('/',    ['as' => 'admin.home', 'uses' => 'HomeController@home']);
    Route::get ('/map', ['as' => 'admin.map',  'uses' => 'HomeController@map']);
    Route::post('/send/message', ['as' => 'admin.send.message', 'uses' => 'HomeController@send_message']);


    Route::get ('/auth/settings',        ['as' => 'admin.auth.settings',        'uses' => 'AuthController@settings']);
    Route::get ('/auth/password',        ['as' => 'admin.auth.password',        'uses' => 'AuthController@password']);
    Route::post('/auth/password/update', ['as' => 'admin.auth.password.update', 'uses' => 'AuthController@password_update']);
    Route::get ('/auth/theme',           ['as' => 'admin.auth.theme',           'uses' => 'AuthController@theme']);
    Route::post('/auth/theme/update',    ['as' => 'admin.auth.theme.update',    'uses' => 'AuthController@theme_update']);

    /* general settings */
    Route::get ('/general/settings',        ['as' => 'admin.general.settings',        'uses' => 'GeneralController@settings']);
    Route::get ('/general/location',        ['as' => 'admin.general.location',        'uses' => 'GeneralController@password']);
    Route::post('/general/location/update', ['as' => 'admin.general.location.update', 'uses' => 'GeneralController@password_update']);

    Route::get ('/general/push',        ['as' => 'admin.general.push',        'uses' => 'GeneralController@push']);
    Route::post('/general/push/update', ['as' => 'admin.general.push.update', 'uses' => 'GeneralController@push_update']);

    /* admin user routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/user/list', ['as' => 'admin.user.list', 'uses' => 'UserController@list']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/user/list',          'UserController@list');
        Route::post('/user/create',        'UserController@create');
        Route::post('/user/update/{user}', 'UserController@update');
        Route::post('/user/delete/{user}', 'UserController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */

    /* admin school routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/school/list', ['as' => 'admin.school.list', 'uses' => 'SchoolController@list']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/school/list',            'SchoolController@list');
        Route::post('/school/create',          'SchoolController@create');
        Route::post('/school/update/{school}', 'SchoolController@update');
        Route::post('/school/delete/{school}', 'SchoolController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */


    /* admin parent routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/parent/list', ['as' => 'admin.parent.list', 'uses' => 'ParentController@list']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/parent/list',            'ParentController@list');
        Route::post('/parent/create',          'ParentController@create');
        Route::post('/parent/update/{parent}', 'ParentController@update');
        Route::post('/parent/delete/{parent}', 'ParentController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */

    /* admin driver routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/driver/list',               ['as' => 'admin.driver.list',      'uses' => 'DriverController@list']);
    Route::get ('/driver/map/{driver}',       ['as' => 'admin.driver.map',       'uses' => 'DriverController@map']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/driver/list',            'DriverController@list');
        Route::get ('/driver/map',             'DriverController@map');
        Route::post('/driver/create',          'DriverController@create');
        Route::post('/driver/update/{driver}', 'DriverController@update');
        Route::post('/driver/delete/{driver}', 'DriverController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */

    /* admin student routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/student/list',             ['as' => 'admin.student.list',   'uses' => 'StudentController@list']);
    Route::get ('/student/image/{student}',  ['as' => 'admin.student.image',  'uses' => 'StudentController@image']);
    Route::post('/student/upload/{student}', ['as' => 'admin.student.upload', 'uses' => 'StudentController@upload']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/student/list',             'StudentController@list');
        Route::post('/student/create',           'StudentController@create');
        Route::post('/student/update/{student}', 'StudentController@update');
        Route::post('/student/delete/{student}', 'StudentController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */

    /* admin payment routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/payment/list', ['as' => 'admin.payment.list', 'uses' => 'PaymentController@list']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/payment/list',             'PaymentController@list');
        Route::post('/payment/create',           'PaymentController@create');
        Route::post('/payment/update/{payment}', 'PaymentController@update');
        Route::post('/payment/delete/{payment}', 'PaymentController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */


    /* admin action acitons routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/action/list', ['as' => 'admin.action.list', 'uses' => 'ActionController@list']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        Route::get ('/action/list',            'ActionController@list');
        Route::post('/action/create',          'ActionController@create');
        Route::post('/action/update/{action}', 'ActionController@update');
        Route::post('/action/delete/{action}', 'ActionController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */


    /* admin message acitons routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/message/list', ['as' => 'admin.message.list', 'uses' => 'MessageController@list']);

    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        /* Students API */
        Route::get ('/message/list',             'MessageController@list');
        Route::post('/message/create',           'MessageController@create');
        Route::post('/message/update/{message}', 'MessageController@update');
        Route::post('/message/delete/{message}', 'MessageController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */


    /* admin news actions routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/news/list', ['as' => 'admin.news.list', 'uses' => 'NewsController@list']);
    Route::get ('/news/image/{news}',  ['as' => 'admin.news.image',  'uses' => 'NewsController@image']);
    Route::post('/news/upload/{news}', ['as' => 'admin.news.upload', 'uses' => 'NewsController@upload']);
    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
        /* Students API */
        Route::get ('/news/list',             'NewsController@list');
        Route::post('/news/create',           'NewsController@create');
        Route::post('/news/update/{news}', 'NewsController@update');
        Route::post('/news/delete/{news}', 'NewsController@delete');
    });
    /* ------------------------------------------------------------------------------------------------------------------ */

    /* admin news actions routes ---------------------------------------------------------------------------------------------- */
    Route::get ('/banner/list', ['as' => 'admin.banner.list', 'uses' => 'BannerController@list']);
    Route::post('/banner/upload', ['as' => 'admin.banner.upload', 'uses' => 'BannerController@upload']);
    Route::post('/banner/delete', ['as' => 'admin.banner.delete', 'uses' => 'BannerController@delete']);

    /* ------------------------------------------------------------------------------------------------------------------ */
});
