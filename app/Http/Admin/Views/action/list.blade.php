@extends('Admin::layout-dx')
@section('title', $title)
@section('content')
<div id="grid"></div>
@stop
@section('script')
@include('Admin::partials.dx.variables')
@include('Admin::partials.dx.functions')
@include('Admin::partials.dx.menu.datepicker')
@include('Admin::partials.dx.dropboxes.driver')
@include('Admin::partials.dx.dropboxes.student')
@include('Admin::' . $type . '.grid')
@stop
