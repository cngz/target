<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "status",
         dataType: "boolean",
        allowSorting: false,
        width: 100,
        alignment: 'center',
        caption: '#',
        lookup: {
            dataSource: ACTION_STATUSES,
            displayExpr: "name",
            valueExpr: "id",
        },
        cellTemplate: function(container, options) {
            if(options.data && options.data.status == 1)
                $('<i class="far fa-check-square fa-lg fa-dx" style="color: green" />').appendTo(container)
            else
                $('<i class="far fa-minus-square fa-lg fa-dx" style="color: red" />').appendTo(container)
        },
        editCellTemplate: function(cellElement, cellInfo) {
            return $('<div class="dx-form-size"/>').dxSwitch({
                value: cellInfo.value || cellInfo.value == undefined,
                onText: '{{ trans('site.Came') }}',
                offText: '{{ trans('site.Not came') }}',
                width: '100%',
                onContentReady: function(e) {
                    if(cellInfo.value == undefined)
                        cellInfo.setValue(1)
                },
                onValueChanged: function(item) {
                    cellInfo.setValue(item.value ? 1 : 0)
                },
            }).appendTo(cellElement)
        },
    },
    {
        dataField: "student_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Student') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select student') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_students()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            student_dropbox(cellElement, cellInfo, get_students())
        },
    },
    {
        dataField: "driver_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Driver') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select driver') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_drivers()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            driver_dropbox(cellElement, cellInfo, get_drivers())
        },
    },
    {
        dataField: "type",
        dataType: "string",
        allowSorting: true,
        width: 250,
        alignment: 'left',
        caption: '{{ trans('site.Type') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select type') }}',
        }],
        lookup: {
            dataSource: ACTION_TYPES,
            displayExpr: "name",
            valueExpr: "id",
        },
    },
    {
        dataField: "take_date",
        dataType: "datetime",
        // allowSorting: false,
        format: "HH:mm, dd.MM.yyyy",
        editorOptions: {
            acceptCustomValue: false,
            openOnFieldClick: true,
            displayFormat: "HH:mm, dd.MM.yyyy",
            // dateSerializationFormat: "yyyy-MM-dd HH:mm:ss",
        },
        caption: '{{ trans("site.Take date") }}',
        // alignment: "center",
        width: 160,
        validationRules: [{
            type: "required",
            message: '{{ trans("site.Select take date") }}',
        }],
        cellTemplate: function(container, options) {
            var text = options.data && options.data.status == 0 ? 'text-danger' : 'text-success'

            var div = $('<div />')
            div.addClass(text).text(options.data.take_date)

            div.appendTo(container)
        },
    },
    {
        dataField: "leave_date",
        dataType: "datetime",
        // allowSorting: false,
        format: "HH:mm, dd.MM.yyyy",
        editorOptions: {
            acceptCustomValue: false,
            openOnFieldClick: true,
            displayFormat: "HH:mm, dd.MM.yyyy",
            // dateSerializationFormat: "yyyy-MM-dd HH:mm:ss",
        },
        caption: '{{ trans("site.Leave date") }}',
        // alignment: "center",
        width: 160,
        validationRules: [{
            type: "required",
            message: '{{ trans("site.Select leave date") }}',
        }],
        cellTemplate: function(container, options) {
            var text = options.data && options.data.status == 0 ? 'text-danger' : 'text-success'

            var div = $('<div />')
            div.addClass(text).text(options.data.leave_date)

            div.appendTo(container)
        },
    },
]
</script>
