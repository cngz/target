@extends('Admin::map.layout')
@section('title', trans('site.Drivers on map'))
@section('content')
@include('Admin::map.header')
<div id="map" style="height: 100%;"></div>
@endsection
@section('script')
@include('Admin::map.scripts.variables')
@include('Admin::map.scripts.init')
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key') }}&callback=initMap&language=ru&region=KG"></script>
@include('Admin::map.scripts.interval')
@include('Admin::map.scripts.dropdown')
@endsection
