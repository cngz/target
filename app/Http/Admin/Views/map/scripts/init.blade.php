<script>
    function getDrivers() {
        $.ajax({
            type: "get",
            url: "/admin/api/driver/map",
            data: {
                drivers: DRIVER_LIST,
            },
            statusCode: {
                404: function() {
                    window.location.reload()
                }
            }
        }).done(function(drivers) {
            addMarker(drivers)
        })
    }

    var markers = []
    var map = {}

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {lat: 42.872778, lng: 74.5857233},
        })

        /*Add a marker clusterer to manage the markers.*/
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'})

        getDrivers()
    }

    function addMarker(drivers) {
        $.each(markers, function(key, marker) {
            marker.setMap(null)
        })
        markers = [] /* clear markers */


        var infoWindow = new google.maps.InfoWindow({
            maxWidth: 300,
        })


        google.maps.event.addListener(map, 'click', function () {
            if(infoWindow) infoWindow.close() /* if open close previous infowindow */
        })

        /*Add some markers to the map.*/
        /*Note: The code uses the JavaScript Array.prototype.map() method to*/
        /*create an array of markers based on a given "drivers" array.*/
        /*The map() method here has nothing to do with the Google Maps API.*/
        drivers.map(function(driver, i) {
            var marker = new google.maps.Marker({
                mid     : driver.id,
                map     : map,
                title   : driver.name,
                position: {lat: parseFloat(driver.lat), lng: parseFloat(driver.lng)},
            })

            google.maps.event.addListener(marker, 'click', function () {
                var infoHtml = document.createElement('div')
                infoHtml.className  = "cursor-pointer"
                infoHtml.innerHTML  = '<h5 class="mt-3">'+driver.carno+' - '+driver.carname+' - '+driver.school.name+'</h5>'
                infoHtml.innerHTML += '<p>'+driver.name+', '+driver.phone+'</p>'

                infoWindow.setContent(infoHtml)
                infoWindow.open(map, marker)
            })

            markers.push(marker)
        })
    }
</script>