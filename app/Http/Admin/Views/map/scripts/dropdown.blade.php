<script type="text/javascript">
$(function() {
    $("#dropdown-grid").dxDropDownBox({
        value: DRIVER_LIST,
        valueExpr: 'id',
        width: 600,
        placeholder: '{{ trans('site.Select driver') }}',
        // showClearButton: true,
        displayExpr: 'carno',
        dataSource: {
            store: new DevExpress.data.CustomStore({
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return $.getJSON('/admin/api/driver/list')
                },
            }),
        },
        contentTemplate: function(e) {
            return $('<div id="dx-dropdown-driver"/>').dxDataGrid({
                dataSource: e.component.option("dataSource"),
                columns: [
                    {
                        dataField: "school.name",
                        dataType: "string",
                        allowSorting: false,
                        width: 100,
                        caption: '{{ trans('site.School') }}',
                    },
                    {
                        dataField: "name",
                        dataType: "string",
                        allowSorting: false,
                        minWidth: 160,
                        // width: 120,
                        caption: '{{ trans('site.Name') }}',
                    },
                    {
                        dataField: "carname",
                        dataType: "string",
                        allowSorting: false,
                        minWidth: 100,
                        caption: '{{ trans('site.Car name') }}',
                    },
                    {
                        dataField: "carno",
                        dataType: "string",
                        allowSorting: false,
                        minWidth: 100,
                        // width: 240,
                        caption: '{{ trans('site.Car number') }}',
                    },
                ],
                hoverStateEnabled: true,
                filterRow: {
                    // visible: true,
                    applyFilter: "auto",
                },
                errorRowEnabled: true,
                headerFilter: {
                    visible: true,
                    allowSearch: true,
                    height: 400,
                    width: 300,
                },
                // paging: {
                //     pageSize: 10,
                //     enabled: true,
                // },
                // pager: {
                //     showPageSizeSelector: true,
                //     allowedPageSizes: [10, 20, 50],
                //     showInfo: true,
                // },
                columnResizingMode: 'widget',
                wordWrapEnabled: true,
                showColumnLines: true,
                showRowLines: true,
                allowColumnReordering: false,
                allowColumnResizing: true,
                noDataText: "{{ trans('site.No data') }}",
                scrolling: { mode: "infinite" },
                height: 400,
                width: "100%",
                selection: { mode: "multiple" },
                onContentReady: function(dropdown) {
                    dropdown.component.selectRows(DRIVER_LIST)
                },
                onSelectionChanged: function(selectedItems) {
                    DRIVER_LIST = selectedItems.selectedRowKeys

                    if(window.sessionStorage) {
                        sessionStorage.setItem('DRIVER_LIST', JSON.stringify(DRIVER_LIST))
                    }

                    getDrivers()
                    e.component.option('value', selectedItems.selectedRowKeys)
                }
            })
        }
    })
})
</script>