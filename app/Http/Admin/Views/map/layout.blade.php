<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', '') }} - @yield('title')</title>
<link rel="stylesheet" type="text/css" href="{{ mix('/assets/panel.css') }}" />
<link rel="dx-theme" data-theme="generic.{{ auth()->user()->theme }}" href="/assets/themes/{{ auth()->user()->theme }}.css" />
<link rel="dx-theme" data-theme="android5.light" href="/assets/themes/android5.light.css" />
<link rel="dx-theme" data-theme="ios7.default" href="/assets/themes/ios7.default.css" />
@yield('style')
<style type="text/css">
    #dx-dropdown-driver .dx-command-select {
    width: 40px;
    min-width: 40px;
}
</style>
</head>
<body class="dx-viewport dx-theme-{{ auth()->user()->theme }}" style="padding-top: 54px;">
@yield('content')
<script type="text/javascript" src="{{ mix('/assets/panel.js') }}"></script>
@yield('script')
</body>
</html>