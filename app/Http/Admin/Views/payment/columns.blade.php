<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "parent_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Parent') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select parent') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_parents()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            parent_dropbox(cellElement, cellInfo, get_parents())
        },
    },
    {
        dataField: "amount",
        dataType: "number",
        allowSorting: true,
        width: 150,
        alignment: 'left',
        caption: '{{ trans('site.Amount') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter amount') }}',
        }],
        format: {
            precision: 2,
        },
    },
    {
        dataField: "date",
        dataType: "date",
        // allowSorting: false,
        format: "dd.MM.yyyy",
        editorOptions: {
            acceptCustomValue: false,
            openOnFieldClick: true,
            displayFormat: "dd.MM.yyyy",
            // dateSerializationFormat: "yyyy-MM-dd",
        },
        caption: '{{ trans("site.Date") }}',
        // alignment: "center",
        width: 160,
        validationRules: [{
            type: "required",
            message: '{{ trans("site.Select date") }}',
        }],
    },
    {
        dataField: "desc",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Notes') }}',
    },
]
</script>