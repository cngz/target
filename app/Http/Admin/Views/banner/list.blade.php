@extends('Admin::layout')
@section('title', trans('site.Image upload'))
@section('content')



    <div class="container mt-4 mb-4">
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-primary  mb-3" data-toggle="modal" data-target="#exampleModal">
                    Загрузить изображения
                </button>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form method="post" action="{{ route('admin.banner.upload')}}" enctype="multipart/form-data" class="form-inline" id="image-form">

                    <div class="modal-content">
                            <div class="modal-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="coord-x" id="coord-x" value="">
                                <input type="hidden" name="coord-y" id="coord-y" value="">
                                <input type="hidden" name="coord-w" id="coord-w" value="">
                                <input type="hidden" name="coord-h" id="coord-h" value="">

                                <div class="form-group">
                                    <input type="file" name="file" accept="image/*" id="file" class="" onchange="setImage(this.files[0]);" required
                                           oninvalid="this.setCustomValidity('{{ trans('site.Select image') }}')"
                                           oninput="this.setCustomValidity('')">

                                </div>

                                <hr>
                                <div id="img-preview-div">
                                    <img src="{{ rand(10000, 99999)}}" id="img-preview" style="width: 100%" onerror="this.src='/img/noimg.png'"/>
                                </div>

                                <div class="text-lowercase text-muted mt-2 mb-2">{{ trans('site.Maximum allowed image size is 4mb') }}</div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary ml-auto">загрузить изображения</button>
                            </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="row">
            @foreach($images as $image)
                <div class="col-3">
                    <form method="post" action="{{ route('admin.banner.delete') }}" class="w-100  border border-info p-3">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="filename" value="{{$image}}">
                        <div class="form-group">
                            <hr>
                            <img src="/{{$image}}" style="width: 100%"/>
                            <button type="submit" class="btn btn-outline-danger w-100 mt-3">Удалить изображения</button>
                        </div>
                    </form>
                </div>
            @endforeach
        </div>
    </div>

@stop

@section('style')
    <link rel="stylesheet" type="text/css" href="/vendor/jcrop/css/jquery.Jcrop.min.css">

@stop

@section('script')
    <script type="text/javascript" src="/vendor/jcrop/js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript">
        var FILEOK = false

        function updateCoords(c) {
            document.getElementById("coord-x").value = Math.floor(c.x)
            document.getElementById("coord-y").value = Math.floor(c.y)
            document.getElementById("coord-w").value = Math.floor(c.w)
            document.getElementById("coord-h").value = Math.floor(c.h)
        }

        function setCoords(obj) {
            var dim = obj.getBounds()
            obj.setSelect([0, 0, Math.floor(dim[0]), Math.floor(dim[1])])
        }

        function initJcrop() {
            FILEOK = true

            $("#img-preview").Jcrop({
                minSize: [150, 150],
                aspectRatio: 4/3,
                boxHeight: 300,
                onSelect: updateCoords
            }, function () {
                setCoords(this);
            })
        }

        function setImage(file) {
            var types = ['image/jpeg', 'image/jpg', 'image/png']
            FILEOK = false

            if (file == undefined) {
                swal('{{ trans('site.Attention') }}', '{{ trans('site.Select valid image') }}', 'error')
            } else if (file.size >= 4 * 1024 * 1024) {
                swal('{{ trans('site.Attention') }}', '{{ trans('site.Image size is too big') }}', 'error')
            } else if (types.indexOf(file.type) < 0) {
                swal('{{ trans('site.Attention') }}', '{{ trans('site.Wrong image type') }}', 'error')
            } else {
                var reader = new FileReader()
                reader.readAsDataURL(file)
                reader.onload = function () {
                    document.getElementById("img-preview-div").innerHTML = '<img src="' + reader.result + '" id="img-preview"/>'
                    initJcrop()
                }
            }
        }

        $('#image-form').on('submit', function (e) {
            if (FILEOK == false) {
                e.preventDefault()
                swal('{{ trans('site.Attention') }}', '{{ trans('site.Select valid image') }}', 'error')
            }
        })
    </script>
@stop