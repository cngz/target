@extends('Admin::layout')
@section('title', trans('site.Change password'))
@section('content')
<div class="container mt-4 mb-5">
    <div class="row">
        <div class="col-sm-6">
            @include('messages.flash')
            <form action="{{ route('admin.auth.password.update') }}" method="post" class="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <h3>{{ trans('site.Enter current password') }}</h3>
                    <input type="password" name="current_password" class="form-control" required autofocus
                    oninvalid="this.setCustomValidity('{{ trans('site.Enter current password') }}')"
                    oninput="this.setCustomValidity('')"
                    >
                </div>
                <div class="form-group">
                    <h3>{{ trans('site.Enter new password') }}</h3>
                    <input type="password" name="password" class="form-control" required
                    oninvalid="this.setCustomValidity('{{ trans('site.Enter new password') }}')"
                    oninput="this.setCustomValidity('')"
                    >
                </div>
                <div class="form-group">
                    <h3>{{ trans('site.Repeat new password') }}</h3>
                    <input type="password" name="password_confirmation" class="form-control" required
                    oninvalid="this.setCustomValidity('{{ trans('site.Repeat new password') }}')"
                    oninput="this.setCustomValidity('')"
                    >
                </div>
                <br>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-outline-primary btn-block text-uppercase">
                                <i class="fa fa-check-square"></i> &nbsp; {{ trans('site.Save') }}
                            </button>
                        </div>
                        <div class="col">&nbsp;</div>
                        <div class="col">
                            <a class="btn btn-outline-secondary btn-block text-uppercase" href="{{ route('admin.auth.settings') }}">
                                {{ trans('site.Back') }} &nbsp; <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
