@extends('Admin::layout')
@section('title', trans('site.Change password'))
@section('content')

<div class="container mt-4 mb-5">
    <div class="row">
        <div class="col-sm-12 form-group">
            @include('messages.flash')
        </div>

        <form action="{{ route('admin.general.push.update') }}" method="post" class="w-100">
            {{ csrf_field() }}
            <div class="col-sm-12 form-group">
                <h3>Title</h3>
                <input type="text" name="title" value="{{ old('title', '') }}" class="form-control" required>
            </div>
            <div class="col-sm-12 form-group">
                <h3>Message</h3>
                <input type="text" name="message" value="{{ old('message', '') }}" class="form-control" required>
            </div>
            <div class="col-sm-12 form-group">
                <h3>Token</h3>
                <input type="text" name="token" value="{{ old('token', '') }}" class="form-control" required>
            </div>
            <div class="col-sm-12 form-group">
                <h3>Type</h3>
                <select name="type" class="form-control" required>
                    <option value="action" @if(old('type', '') == 'action') selected @endif>Action</option>
                    <option value="message" @if(old('type', '') == 'message') selected @endif>Message</option>
                </select>
            </div>
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-block" style="max-width: 200px;">SEND</button>
            </div>
        </form>
    </div>
</div>
@endsection
