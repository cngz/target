@extends('Admin::layout')
@section('title', trans('site.Image upload'))
@section('content')

<div class="container mt-4 mb-4">
    @include('messages.flash')
    <form method="post" action="{{ route('admin.student.upload', $student) }}" enctype="multipart/form-data" class="form-inline" id="image-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="coord-x" id="coord-x" value="">
        <input type="hidden" name="coord-y" id="coord-y" value="">
        <input type="hidden" name="coord-w" id="coord-w" value="">
        <input type="hidden" name="coord-h" id="coord-h" value="">

        <div class="form-group">
            <input type="file" name="file" accept="image/*" id="file" class="" onchange="setImage(this.files[0]);" style="width: 300px" required
                oninvalid="this.setCustomValidity('{{ trans('site.Select image') }}')"
                oninput="this.setCustomValidity('')">

            <button type="submit" class="btn btn-outline-primary">загрузить изображения</button>
        </div>
    </form>

    <hr>
    <div id="img-preview-div">
        <img src="{{ $student->image }}?{{ rand(10000, 99999)}}" id="img-preview" style="height: 300px" onerror="this.src='/img/noimg.png'" />
    </div>

    <div class="text-lowercase text-muted mt-2 mb-2">{{ trans('site.Maximum allowed image size is 4mb') }}</div>
</div>

@stop

@section('style')
<link rel="stylesheet" type="text/css" href="/vendor/jcrop/css/jquery.Jcrop.min.css">

@stop

@section('script')
<script type="text/javascript" src="/vendor/jcrop/js/jquery.Jcrop.min.js"></script>
<script type="text/javascript">
    var FILEOK = false

    function updateCoords(c)
    {
        document.getElementById("coord-x").value = Math.floor(c.x)
        document.getElementById("coord-y").value = Math.floor(c.y)
        document.getElementById("coord-w").value = Math.floor(c.w)
        document.getElementById("coord-h").value = Math.floor(c.h)
    }

    function setCoords(obj)
    {
        var dim = obj.getBounds()
        obj.setSelect([0, 0, Math.floor(dim[0]), Math.floor(dim[1])])
    }

    function initJcrop()
    {
        FILEOK = true

        $("#img-preview").Jcrop({
            aspectRatio: 1,
            minSize: [150,150],
            boxHeight: 300,
            onSelect: updateCoords
        }, function () {
            setCoords(this);
        })
    }

    function setImage(file)
    {
        var types = ['image/jpeg', 'image/jpg', 'image/png']
        FILEOK = false

        if(file == undefined) {
            swal('{{ trans('site.Attention') }}', '{{ trans('site.Select valid image') }}', 'error')
        } else if(file.size >= 4 * 1024 * 1024) {
            swal('{{ trans('site.Attention') }}', '{{ trans('site.Image size is too big') }}', 'error')
        } else if(types.indexOf(file.type) < 0) {
            swal('{{ trans('site.Attention') }}', '{{ trans('site.Wrong image type') }}', 'error')
        } else {
            var reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onload = function() {
                document.getElementById("img-preview-div").innerHTML = '<img src="' + reader.result + '" id="img-preview"/>'
                initJcrop()
            }
        }
    }

    $('#image-form').on('submit', function(e) {
        if(FILEOK == false) {
            e.preventDefault()
            swal('{{ trans('site.Attention') }}', '{{ trans('site.Select valid image') }}', 'error')
        }
    })
</script>
@stop