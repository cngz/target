<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "name",
        dataType: "string",
        allowSorting: false,
        minWidth: 260,
        caption: '{{ trans('site.Name') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter name') }}',
        }],
    },
    {
        dataField: "phone",
        dataType: "string",
        allowSorting: false,
        width: 140,
        caption: '{{ trans('site.Phone') }}',
    },
    {
        dataField: "parent_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Parent') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select parent') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_parents()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            parent_dropbox(cellElement, cellInfo, get_parents())
        },
    },
    {
        dataField: "driver_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Driver') }}',
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_drivers()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            driver_dropbox(cellElement, cellInfo, get_drivers())
        },
    },
    {
        dataField: "school_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.School') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select school') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_schools()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            school_dropbox(cellElement, cellInfo, get_schools())
        },
    },
    {
        dataField: "desc",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Notes') }}',
    },
]
</script>