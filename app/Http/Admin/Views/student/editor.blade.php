<script type="text/javascript">
const DATA_EDITOR = {
    allowAdding: true,
    mode: "popup", /* row, batch, form */
    popup: {
        title: '{{ trans('site.Edit') }}',
        showTitle: true,
        width: "70%",
        height: "70%",
        shading: true,
        shadingColor: "rgba(0, 0, 0, 0.6)",
        position: {
            my: "center",
            at: "center",
            of: window
        }
    },
    form: {
        width: "100%",
        height: "100%",
        elementAttr: {
            'id': 'dx-{{ $type }}-popup-form',
        },
        labelLocation: "top",
        showColonAfterLabel: false,
        colCount: 6,
        items: [
            {
                colSpan: 5,
                dataField: 'name',
                editorOptions: {
                    maxLength: 60,
                }
            },
            {
                colSpan: 1,
                dataField: 'phone',
                editorOptions: {
                    mask: "000000000",
                    maxLength: 9,
                }
            },
            {
                colSpan: 6,
                dataField: 'school_id',
            },
            {
                colSpan: 6,
                dataField: 'parent_id',
            },
            {
                colSpan: 6,
                dataField: 'driver_id',
            },
            {
                colSpan: 6,
                dataField: 'desc',
                editorOptions: {
                    maxLength: 250,
                }
            },
        ]
    },
    texts: CONFIRM_TEXTS,
}
</script>