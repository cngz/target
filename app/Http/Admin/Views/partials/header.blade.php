<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
    <a class="navbar-brand" href="{{ route('admin.home') }}"><i class="fa fa-home"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ auth()->user()->name }}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="left: auto; right: 4px;">
                    <a class="dropdown-item" href="javascript:document.getElementById('logout-form').submit()"><i class="fa fa-power-off"></i> &nbsp;{{ trans('site.Logout') }}</a>
                </div>
            </li>
        </ul>
    </div>
    </div>
</nav>

<form action="{{ route('logout') }}" method="post" id="logout-form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>