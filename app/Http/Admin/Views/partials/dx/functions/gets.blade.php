<script type="text/javascript">
function get_schools() {
    if(DATA_SCHOOLS.length > 0)
        return DATA_SCHOOLS
    else
        return $.getJSON("/admin/api/school/list", function(data) {
            DATA_SCHOOLS = data
        })
}

function get_students() {
    if(DATA_STUDENTS.length > 0)
        return DATA_STUDENTS
    else
        return $.getJSON("/admin/api/student/list", function(data) {
            DATA_STUDENTS = data
        })
}

function get_parents() {
    if(DATA_PARENTS.length > 0)
        return DATA_PARENTS
    else
        return $.getJSON("/admin/api/parent/list", function(data) {
            DATA_PARENTS = data
        })
}

function get_drivers() {
    if(DATA_DRIVERS.length > 0)
        return DATA_DRIVERS
    else
        return $.getJSON("/admin/api/driver/list", function(data) {
            DATA_DRIVERS = data
        })
}

function get_users() {
    if(DATA_USERS.length > 0)
        return DATA_USERS
    else
        return $.getJSON("/admin/api/user/list", function(data) {
            DATA_USERS = data
        })
}
</script>