<script type="text/javascript">
function error_swal(request) {
    var errors = []
    var title  = '{{ trans('site.An unknown error occurred, try to refresh the page') }}'

    if(request.hasOwnProperty('responseJSON')) {
        var responseJSON = request.responseJSON
        if(responseJSON.hasOwnProperty('message'))
            title = responseJSON.message

        if(responseJSON.hasOwnProperty('errors')) {
            $.each(responseJSON.errors, function(key, value) {
                $.each(responseJSON.errors[key], function(skey, value) {
                    errors.push(value)
                })
            })
        }

        swal(errors.length > 0 ? '' : title, errors.join('\n'), 'error')
    } else {
        if(request.hasOwnProperty('status'))
            title = request.status + ''
        if(request.hasOwnProperty('statusText'))
            errors.push(request.statusText)

        swal(title, errors.join('\n'), 'error')
    }
}
</script>