<script type="text/javascript">
var FILTER_VALUES  = null
var MASTER_DETAIL  = false
var EXPORT_GRID    = false
var COLUMN_CHOOSER = false
var IS_EXPANED     = false

var DATA_SCHOOLS   = []
var DATA_PARENTS   = []
var DATA_DRIVERS   = []
var DATA_STUDENTS  = []
var DATA_USERS     = []

var RECEIVERS = []

var STATE_STORING  = '{{ isprod() }}'

const USER_STATUSES = [
    {'id': 1,   'name': '{{ trans('site.Active') }}'},
    {'id': 0, 'name': '{{ trans('site.Not active') }}'},
]

const USER_ROLES = [
    {'id': 'manager', 'name': '{{ trans('site.Manager') }}'},
    {'id': 'admin',   'name': '{{ trans('site.Administrator') }}'},
]

const ACTION_STATUSES = [
    {'id': 1, 'name': '{{ trans('site.Came') }}'},
    {'id': 0, 'name': '{{ trans('site.Not came') }}'},
]

const CHECK_STATUSES = [
    {'id': 2, 'name': '{{ trans('site.Checked') }}'},
    {'id': 1, 'name': '{{ trans('site.Not checked') }}'},
]

const CHECK_TYPES = [
    {'id': 'h2s', 'name': '{{ trans('site.Home to school') }}'},
    {'id': 's2h', 'name': '{{ trans('site.School to home') }}'},
]

const ACTION_TYPES = [
    {'id': 'h2s', 'name': '{{ trans('site.Home to school') }}'},
    {'id': 's2h', 'name': '{{ trans('site.School to home') }}'},
]

const RECEIVER_TYPES = [
    {'id': 'parent', 'name': '{{ trans('site.Parent') }}'},
    {'id': 'driver', 'name': '{{ trans('site.Driver') }}'},
]

const CONFIRM_TEXTS = {
    confirmDeleteTitle: '{{ trans('site.Attention') }}',
    confirmDeleteMessage: '{{ trans('site.Are you sure?') }}',
    saveRowChanges: '{{ trans('site.Save') }}',
    cancelRowChanges: '{{ trans('site.Back') }}',
}
</script>