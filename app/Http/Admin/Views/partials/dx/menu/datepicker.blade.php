<script type="text/javascript">
function menu_datepicker(grid) {
    return $('<div/>').dxDateBox({
        value: JSDATE,
        openOnFieldClick: true,
        pickerType: 'rollers',
        displayFormat: 'dd MMM yyyy',
        width: 200,
        onValueChanged: function(data) {
            JSDATE = data.value
            grid.refresh()
        }
    })
}
</script>
