<script type="text/javascript">
function check_logs_popup(items) {
    $("#popup").empty()
    var rowEdited = false
    var popup = $("#popup").dxPopup({
        width: "50%",
        height: "90%",
        shading: true,
        shadingColor: "rgba(0, 0, 0, 0.6)",
        contentTemplate: function(container) {
            var popupGridElement = $('<div />')
            var popupGrid =  popupGridElement.dxDataGrid({
                dataSource: items,
                keyExpr: "id",
                // remoteOperations: {
                //     sorting: true,
                //     paging: true,
                //     filtering: true,
                // },
                paging: {
                    pageSize: 10,
                    enabled: true,
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 20, 50, 100],
                    showInfo: true,
                },
                // loadPanel: {
                //     enabled: false,
                // },
                columns: [
                    {
                        dataField: "type",
                        dataType: "string",
                        allowSorting: true,
                        // width: 150,
                        caption: '{{ trans('site.Type') }}',
                        lookup: {
                            dataSource: CHECK_TYPES,
                            displayExpr: "name",
                            valueExpr: "id",
                        },
                    },
                    {
                        dataField: "checked",
                        dataType: "boolean",
                        allowSorting: false,
                        // width: 100,
                        alignment: 'center',
                        caption: '#',
                        lookup: {
                            dataSource: CHECK_STATUSES,
                            displayExpr: "name",
                            valueExpr: "id",
                        },
                        cellTemplate: function(container, options) {
                            if(options.data && options.data.checked == 1)
                                $('<i class="far fa-check-square fa-lg fa-dx" style="color: green" />').appendTo(container)
                            else
                                $('<i class="far fa-minus-square fa-lg fa-dx" style="color: gray" />').appendTo(container)
                        },
                    },
                    {
                        dataField: "create_date",
                        dataType: "datetime",
                        // allowSorting: false,
                        format: "HH:mm, dd.MM.yyyy",
                        caption: '{{ trans("site.Date") }}',
                        // alignment: "center",
                        // width: 160,
                    },
                ],
                filterRow: {
                    visible: true,
                    applyFilter: "auto",
                },
                wordWrapEnabled: true,
                headerFilter: {
                    visible: true,
                    allowSearch: true,
                    height: 400,
                    width: 300,
                },
                columnMinWidth: 20,
            visible: true,
                columnResizingMode: 'widget',
                columnHidingEnabled: false,
                // columnAutoWidth: true,
                scrolling: {
                    // mode: "virtual" /* infinite */
                },
                showColumnLines: true,
                showRowLines: true,
                rowAlternationEnabled: true,
                //showBorders: true,
                width: "100%",
                height: "100%",
                allowColumnReordering: false,
                allowColumnResizing: true,
                selection: {
                    mode: "single", /* multiple */
                },
                hoverStateEnabled: true,
                grouping: {
                    // autoExpandAll: false
                },
                // searchPanel: {
                //     visible: true,
                //     width: 240,
                //     placeholder: "Search...",
                // },
                onToolbarPreparing: function(e) {
                    e.toolbarOptions.items.push(
                        {
                            location: "before",
                            template: function(){
                                return $("<h4/>").html('{{ trans('site.History') }}')
                            }
                        },
                        {
                            location: "after",
                            widget: "dxButton",
                            options: {
                                text: '{!! trans("site.Clear filters") !!}',
                                icon: 'refresh',
                                // onInitialized: function(e) {
                                // },
                                onClick: function(e) {
                                    popupGrid.clearFilter()
                                    popupGrid.refresh()
                                    //e.component.option("disabled", true)
                                }
                            }
                        },
                        {
                            location: "after",
                            widget: "dxButton",
                            options: {
                                text: '{{ trans('site.Close') }}',
                                icon: 'close',
                                // onInitialized: function(e) {
                                // },
                                onClick: function(e) {
                                    popup.hide()
                                    //e.component.option("disabled", true)
                                }
                            }
                        },
                    )
                },
            }).dxDataGrid('instance')

            popupGridElement.appendTo(container)
        },
        showTitle: false,
        title: '{{ trans('site.History') }}',
        visible: false,
        dragEnabled: false,
        closeOnOutsideClick: false,
        position: {
            my: "center",
            at: "center",
            of: window
        },
        onHiding: function (options) {
            if(rowEdited)
                $('#grid').dxDataGrid('instance').refresh()

            rowEdited = false
        },
    }).dxPopup("instance");
    popup.show();
};
</script>