<script type="text/javascript">
function send_message(type, receivers) {
    var title = ''

    if(type == 'parent') {
        title = '{{ trans('site.Parents will receive message') }}'
    } else {
        title = '{{ trans('site.Drivers will receive message') }}'
    }

    formData = {
        type      : type,
        title     : '',
        content   : '',
        receivers : receivers,
    }

    $("#popup").empty()
    var popup = $("#popup").dxPopup({
        width: "60%",
        height: 300,
        shading: true,
        shadingColor: "rgba(0, 0, 0, 0.6)",
        contentTemplate: function(container) {
            var popupElement = $('<div />')
            popupElement.dxForm({
                scrollingEnabled: false,
                colCount: 1,
                labelLocation: 'top',
                showColonAfterLabel: false,
                validationGroup: "formData",
                formData: formData,
                items: [
                    {
                        dataField: "title",
                        dataType: "string",
                        editorType: "dxTextBox",
                        editorOptions: {
                            placeholder: "{{ trans('site.Enter title') }}",
                            maxLength: 100,
                        },
                        label: {
                            text: '{{ trans('site.Title') }}'
                        },
                        validationRules: [{
                            type: "required",
                            message: "{{ trans('site.Enter title') }}"
                        }],
                    },
                    {
                        dataField: "content",
                        dataType: "string",
                        editorType: "dxTextBox",
                        editorOptions: {
                            placeholder: "{{ trans('site.Enter message') }}",
                            maxLength: 250,
                        },
                        label: {
                            text: '{{ trans('site.Message') }}'
                        },
                        validationRules: [{
                            type: "required",
                            message: "{{ trans('site.Enter message') }}"
                        }],
                    },
                    {
                        editorType: "dxButton",
                        alignment: 'right',
                        editorOptions: {
                            width: 200,

                            text: '{{ trans('site.Send message') }}',
                            type: "primary",
                            validationGroup: "formData",
                            onClick: function (e) {
                                var validation = e.validationGroup.validate()
                                if(validation.isValid) {
                                    popup.hide()
                                    $.ajax({
                                        url: '{{ route('admin.send.message') }}',
                                        method: "POST",
                                        data: formData,
                                        headers : {'X-CSRF-Token': '{{ csrf_token() }}'},
                                        success: function(response) {
                                            popup.hide()
                                        },
                                        error: function (request, status, error) {
                                            swal('', request.responseJSON.message || error, 'error')
                                        }
                                    })
                                }
                            },
                        },
                    },
                ] /* end of items */
            })

            popupElement.appendTo(container)
        },
        showTitle: true,
        title: receivers.length + ' - ' + title,
        visible: false,
        dragEnabled: false,
        closeOnOutsideClick: false,
        position: {
            my: "center",
            at: "center",
            of: window
        },
        onHiding: function (options) {
        },
    }).dxPopup("instance");
    popup.show();
};
</script>