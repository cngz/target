<script type="text/javascript">
function actions_popup(driver) {
    $("#popup").empty()
    var rowEdited = false
    var popup = $("#popup").dxPopup({
        width: "82%",
        height: "80%",
        shading: true,
        shadingColor: "rgba(0, 0, 0, 0.6)",
        contentTemplate: function(container) {
            var popupGridElement = $('<div />')
            var popupGrid =  popupGridElement.dxDataGrid({
                dataSource: new DevExpress.data.CustomStore({
                    key: "id",
                    loadMode: "raw",
                    load: function(loadOptions) {
                        return $.getJSON('/admin/api/action/list?date=' + JSDATE + '&driver=' + driver.id)
                    }
                }),
                // keyExpr: "id",
                // remoteOperations: {
                //     sorting: true,
                //     paging: true,
                //     filtering: true,
                // },
                paging: {
                    pageSize: 10,
                    enabled: true,
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 20, 50, 100],
                    showInfo: true,
                },
                // loadPanel: {
                //     enabled: false,
                // },
                columns: [
                    {
                        dataField: "status",
                         dataType: "boolean",
                        allowSorting: false,
                        width: 70,
                        alignment: 'center',
                        caption: '#',
                        lookup: {
                            dataSource: ACTION_STATUSES,
                            displayExpr: "name",
                            valueExpr: "id",
                        },
                        cellTemplate: function(container, options) {
                            if(options.data && options.data.status == 1)
                                $('<i class="far fa-check-square fa-lg fa-dx" style="color: green" />').appendTo(container)
                            else
                                $('<i class="far fa-minus-square fa-lg fa-dx" style="color: red" />').appendTo(container)
                        },
                    },
                    {
                        dataField: "student_id",
                        dataType: "string",
                        allowSorting: false,
                        caption: '{{ trans('site.Student') }}',
                        lookup: {
                            dataSource: {
                                key: "id",
                                loadMode: "raw",
                                load: function(loadOptions) {
                                    return get_students()
                                },
                            },
                            searchExpr: ['name'],
                            valueExpr: "id",
                            displayExpr: function(item) {
                                return item && item != undefined ? item.name : ''
                            },
                        },
                    },
                    {
                        dataField: "type",
                        dataType: "string",
                        allowSorting: true,
                        width: 250,
                        alignment: 'left',
                        caption: '{{ trans('site.Type') }}',
                        lookup: {
                            dataSource: ACTION_TYPES,
                            displayExpr: "name",
                            valueExpr: "id",
                        },
                    },
                    {
                        dataField: "take_date",
                        dataType: "datetime",
                        // allowSorting: false,
                        format: "HH:mm, dd.MM.yyyy",
                        editorOptions: {
                            acceptCustomValue: false,
                            openOnFieldClick: true,
                            displayFormat: "HH:mm, dd.MM.yyyy",
                            // dateSerializationFormat: "yyyy-MM-dd HH:mm:ss",
                        },
                        caption: '{{ trans("site.Take date") }}',
                        // alignment: "center",
                        width: 160,
                        cellTemplate: function(container, options) {
                            var text = options.data && options.data.status == 0 ? 'text-danger' : 'text-success'

                            var div = $('<div />')
                            div.addClass(text).text(options.data.take_date)

                            div.appendTo(container)
                        },
                    },
                    {
                        dataField: "leave_date",
                        dataType: "datetime",
                        // allowSorting: false,
                        format: "HH:mm, dd.MM.yyyy",
                        editorOptions: {
                            acceptCustomValue: false,
                            openOnFieldClick: true,
                            displayFormat: "HH:mm, dd.MM.yyyy",
                            // dateSerializationFormat: "yyyy-MM-dd HH:mm:ss",
                        },
                        caption: '{{ trans("site.Leave date") }}',
                        // alignment: "center",
                        width: 160,
                        cellTemplate: function(container, options) {
                            var text = options.data && options.data.status == 0 ? 'text-danger' : 'text-success'

                            var div = $('<div />')
                            div.addClass(text).text(options.data.leave_date)

                            div.appendTo(container)
                        },
                    },
                ],
                filterRow: {
                    visible: true,
                    applyFilter: "auto",
                },
                wordWrapEnabled: true,
                headerFilter: {
                    visible: true,
                    allowSearch: true,
                    height: 400,
                    width: 300,
                },
                columnMinWidth: 20,
                visible: true,
                columnResizingMode: 'widget',
                columnHidingEnabled: false,
                // columnAutoWidth: true,
                scrolling: {
                    // mode: "virtual" /* infinite */
                },
                showColumnLines: true,
                showRowLines: true,
                rowAlternationEnabled: true,
                //showBorders: true,
                width: "100%",
                height: "100%",
                allowColumnReordering: false,
                allowColumnResizing: true,
                selection: {
                    mode: "single", /* multiple */
                },
                hoverStateEnabled: true,
                grouping: {
                    // autoExpandAll: false
                },
                // searchPanel: {
                //     visible: true,
                //     width: 240,
                //     placeholder: "Search...",
                // },
                onToolbarPreparing: function(e) {
                    e.toolbarOptions.items.push(
                        {
                            location: "before",
                            template: function(){
                                return $("<h4/>").html('{{ trans('site.History') }}')
                            }
                        },
                        {
                        location: "center",
                            template: function() {
                                return menu_datepicker(e.component)
                            }
                        },
                        {
                            location: "after",
                            widget: "dxButton",
                            options: {
                                text: '{!! trans("site.Clear filters") !!}',
                                icon: 'refresh',
                                // onInitialized: function(e) {
                                // },
                                onClick: function(e) {
                                    popupGrid.clearFilter()
                                    popupGrid.refresh()
                                    //e.component.option("disabled", true)
                                }
                            }
                        },
                        {
                            location: "after",
                            widget: "dxButton",
                            options: {
                                text: '{{ trans('site.Close') }}',
                                icon: 'close',
                                // onInitialized: function(e) {
                                // },
                                onClick: function(e) {
                                    popup.hide()
                                    //e.component.option("disabled", true)
                                }
                            }
                        },
                    )
                },
            }).dxDataGrid('instance')

            popupGridElement.appendTo(container)
        },
        showTitle: false,
        title: '{{ trans('site.History') }}',
        visible: false,
        dragEnabled: false,
        closeOnOutsideClick: false,
        position: {
            my: "center",
            at: "center",
            of: window
        },
        onHiding: function (options) {
            if(rowEdited)
                $('#grid').dxDataGrid('instance').refresh()

            rowEdited = false
        },
    }).dxPopup("instance");
    popup.show();
};
</script>
