<script type="text/javascript">
function school_dropbox(element, data, items) {
    var dropDownBox = $('<div/>').dxDropDownBox({
        value: data.value,
        valueExpr: 'id',
        width: "100%",
        placeholder: '{{ trans('site.Select school') }}',
        showClearButton: true,
        displayExpr: function(item) {
            return item && item != undefined ? item.name : ''
        },
        onValueChanged: function(item) {
            data.setValue(item.value)
        },
        dataSource: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function(loadOptions) {
                return items
            },
        }),
        contentTemplate: function(e) {
            return $('<div id="dx-dropdown-school"/>').dxDataGrid({
                dataSource: e.component.option("dataSource"),
                columns: [
                    {
                        dataField: "name",
                        dataType: "string",
                        allowSorting: false,
                        // width: 120,
                        caption: '{{ trans('site.Name') }}',
                    },
                    {
                        dataField: "address",
                        dataType: "string",
                        allowSorting: false,
                        caption: '{{ trans('site.Address') }}',
                    },
                    {
                        dataField: "desc",
                        dataType: "string",
                        allowSorting: false,
                        caption: '{{ trans('site.Notes') }}',
                    },
                ],
                hoverStateEnabled: true,
                filterRow: {
                    visible: true,
                    applyFilter: "auto",
                },
                errorRowEnabled: true,
                headerFilter: {
                    visible: true,
                    allowSearch: true,
                    height: 400,
                    width: 300,
                },
                paging: {
                    pageSize: 10,
                    enabled: true,
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 20, 50],
                    showInfo: true,
                },
                showColumnLines: true,
                showRowLines: true,
                allowColumnReordering: false,
                allowColumnResizing: true,
                // scrolling: { mode: "infinite" },
                height: 340,
                width: "100%",
                selection: { mode: "single" },
                onSelectionChanged: function(selectedItems) {
                    var grid = this
                    grid.clearFilter()
                    e.component.option('value', selectedItems.currentSelectedRowKeys[0])
                    e.component.close()
                }
            })
        }
    }).appendTo(element)
}
</script>