<script type="text/javascript">
function user_dropbox(element, data, items) {
    var dropDownBox = $('<div/>').dxDropDownBox({
        value: data.value,
        valueExpr: 'id',
        width: "100%",
        placeholder: '{{ trans('site.Select user') }}',
        showClearButton: true,
        displayExpr: function(item) {
            return item && item != undefined ? item.name : ''
        },
        onValueChanged: function(item) {
            data.setValue(item.value)
        },
        dataSource: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function(loadOptions) {
                return items
            },
        }),
        contentTemplate: function(e) {
            return $('<div id="dx-dropdown-user"/>').dxDataGrid({
                dataSource: e.component.option("dataSource"),
                columns: [
                    {
                        dataField: "name",
                        dataType: "string",
                        allowSorting: false,
                        // width: 120,
                        caption: '{{ trans('site.Name') }}',
                    },
                    {
                        dataField: "phone",
                        dataType: "string",
                        allowSorting: false,
                        width: 140,
                        caption: '{{ trans('site.Phone') }}',
                    },
                    {
                        dataField: "email",
                        dataType: "string",
                        allowSorting: false,
                        // width: 240,
                        caption: '{{ trans('site.Email') }}',
                    },
                    {
                        dataField: "role",
                        dataType: "string",
                        allowSorting: false,
                        // width: 240,
                        caption: '{{ trans('site.Role') }}',
                        lookup: {
                            dataSource: USER_ROLES,
                            displayExpr: "name",
                            valueExpr: "id",
                        },
                    },
                    {
                        dataField: "desc",
                        dataType: "string",
                        allowSorting: false,
                        // width: 240,
                        caption: '{{ trans('site.Notes') }}',
                    },
                ],
                hoverStateEnabled: true,
                filterRow: {
                    visible: true,
                    applyFilter: "auto",
                },
                errorRowEnabled: true,
                headerFilter: {
                    visible: true,
                    allowSearch: true,
                    height: 400,
                    width: 300,
                },
                paging: {
                    pageSize: 10,
                    enabled: true,
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 20, 50],
                    showInfo: true,
                },
                showColumnLines: true,
                showRowLines: true,
                allowColumnReordering: false,
                allowColumnResizing: true,
                // scrolling: { mode: "infinite" },
                height: 340,
                width: "100%",
                selection: { mode: "single" },
                onSelectionChanged: function(selectedItems) {
                    var grid = this
                    grid.clearFilter()
                    e.component.option('value', selectedItems.currentSelectedRowKeys[0])
                    e.component.close()
                }
            })
        }
    }).appendTo(element)
}
</script>