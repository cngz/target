<div id="grid-menu">
    <a href="javascript:void(0)" id="grid-menu-button"><i class="fa fa-chevron-left"></i></a>
    <ul>
        <li>&nbsp;</li>
        <li class="{{ request()->is('admin') ? 'active' : '' }}"><a href="{{ route('admin.home') }}">{{ trans('site.Homepage') }}</a></li>
        <li>&nbsp;</li>
        <li class="divider">&nbsp;</li>
        <li class="{{ request()->is('admin/school/list') ? 'active' : '' }}"><a href="{{ route('admin.school.list') }}">{{ trans('site.Schools') }}</a></li>
        <li class="{{ request()->is('admin/driver/list') ? 'active' : '' }}"><a href="{{ route('admin.driver.list') }}">{{ trans('site.Drivers') }}</a></li>
        <li class="{{ request()->is('admin/parent/list') ? 'active' : '' }}"><a href="{{ route('admin.parent.list') }}">{{ trans('site.Parents') }}</a></li>
        <li class="{{ request()->is('admin/student/list') ? 'active' : '' }}"><a href="{{ route('admin.student.list') }}">{{ trans('site.Students') }}</a></li>
        <li class="divider">&nbsp;</li>
        <li class="{{ request()->is('admin/message/list') ? 'active' : '' }}"><a href="{{ route('admin.message.list') }}">{{ trans('site.Messages') }}</a></li>
        <li class="{{ request()->is('admin/banner/list') ? 'active' : '' }}"><a href="{{ route('admin.banner.list') }}">{{ trans('site.Banner') }}</a></li>
        <li class="{{ request()->is('admin/news/list') ? 'active' : '' }}"><a href="{{ route('admin.news.list') }}">{{ trans('site.News') }}</a></li>
        <li class="{{ request()->is('admin/action/list') ? 'active' : '' }}"><a href="{{ route('admin.action.list') }}">{{ trans('site.History') }}</a></li>
        <li class="{{ request()->is('admin/payment/list') ? 'active' : '' }}"><a href="{{ route('admin.payment.list') }}">{{ trans('site.Payments') }}</a></li>
        <li class="{{ request()->is('admin/map') ? 'active' : '' }}"><a href="{{ route('admin.map') }}">{{ trans('site.Map') }}</a></li>
        <li class="divider">&nbsp;</li>
        @if(auth()->user()->isAdmin())
        <li class="{{ request()->is('admin/user/list') ? 'active' : '' }}"><a href="{{ route('admin.user.list') }}">{{ trans('site.Administrators') }}</a></li>
        @endif
        <li class="{{ request()->is('admin/auth/settings') ? 'active' : '' }}"><a href="{{ route('admin.auth.settings') }}">{{ trans('site.Settings') }}</a></li>
        <li>&nbsp;</li>
    </ul>
</div>
