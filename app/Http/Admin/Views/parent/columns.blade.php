<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "children_count",
        dataType: "string",
        allowSorting: true,
        width: 100,
        alignment: 'center',
        caption: '{{ trans('site.Children') }}'
    },
    {
        dataField: "name",
        dataType: "string",
        allowSorting: false,
        minWidth: 260,
        caption: '{{ trans('site.Name') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter name') }}',
        }],
    },
    {
        dataField: "phone",
        dataType: "string",
        allowSorting: false,
        width: 140,
        caption: '{{ trans('site.Phone') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter phone number') }}',
        }],
    },
    {
        dataField: "start_date",
        dataType: "date",
        // allowSorting: false,
        format: "dd.MM.yyyy",
        editorOptions: {
            acceptCustomValue: false,
            openOnFieldClick: true,
            displayFormat: "dd.MM.yyyy",
            // dateSerializationFormat: "yyyy-MM-dd",
        },
        caption: '{{ trans("site.Start date") }}',
        // alignment: "center",
        width: 110,
        validationRules: [{
            type: "required",
            message: '{{ trans("site.Select start date") }}',
        }],
    },
    {
        dataField: "end_date",
        dataType: "date",
        // allowSorting: false,
        format: "dd.MM.yyyy",
        editorOptions: {
            acceptCustomValue: false,
            openOnFieldClick: true,
            displayFormat: "dd.MM.yyyy",
            // dateSerializationFormat: "yyyy-MM-dd",
        },
        caption: '{{ trans("site.End date") }}',
        // alignment: "center",
        width: 110,
        validationRules: [{
            type: "required",
            message: '{{ trans("site.Select end date") }}',
        }],
    },
    {
        dataField: "fare",
        dataType: "number",
        allowSorting: true,
        visible: false,
        width: 100,
        caption: '{{ trans('site.Agreement amount') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter agreement amount') }}',
        }],
        editCellTemplate: function(cellElement, cellInfo) {
            return $('<div/>').dxNumberBox({
                value: cellInfo.value,
                onContentReady: function(e) {
                    if(cellInfo.value == undefined)
                        cellInfo.setValue(0)
                },
                onValueChanged: function(item) {
                    cellInfo.setValue(item.value)
                },
            }).appendTo(cellElement)
        },
    },
    {
        dataField: "payments_total",
        dataType: "number",
        allowSorting: true,
        visible: false,
        width: 100,
        caption: '{{ trans('site.Total payments') }}',
    },
    {
        dataField: "balance",
        dataType: "number",
        allowSorting: true,
        width: 100,
        caption: '{{ trans('site.Balance') }}',
    },
    {
        dataField: "schools",
        dataType: "string",
        allowSorting: false,
        // width: 100,
        caption: '{{ trans('site.Schools') }}',
        calculateDisplayValue: function(rowData) {
            try {
                return rowData.children.map(function(elem){
                    return elem.school.name;
                }).join(",")
            } catch(err) {
                return ''
            }
        },
    },
    {
        dataField: "address",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Address') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter address') }}',
        }],
    },
    {
        dataField: "desc",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Notes') }}',
    },
]
</script>