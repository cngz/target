@include('Admin::' . $type . '.variables')
@include('Admin::' . $type . '.datasource')
@include('Admin::' . $type . '.columns')
@include('Admin::' . $type . '.editor')
@include('Admin::' . $type . '.detail')
<script type="text/javascript">
$(function() {
var dataGrid = $("#grid").dxDataGrid({
        dataSource: {
            store: DATA_SOURCE,
            filter: FILTER_VALUES,
        },
        // keyExpr: "uuid",
        remoteOperations: {
            // sorting: true,
            // paging: true,
            // filtering: true,
        },
        paging: {
            pageSize: 20,
            enabled: true,
        },
        wordWrapEnabled: true,
        export: {
            enabled: EXPORT_GRID,
            fileName: "{{ $title }} - {{ date('d-m-Y') }}",
            allowExportSelectedData: false
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 20, 50, 100, 1000],
            showInfo: true,
        },
        // loadPanel: {
        //     enabled: false,
        // },
        columns: DATA_COLUMNS,
        editing: DATA_EDITOR,
        filterRow: {
            visible: true,
            applyFilter: "auto",
        },
        headerFilter: {
            visible: true,
            allowSearch: true,
            height: 500,
            width: 400,
        },
        // columnAutoWidth: true,
        scrolling: {
            //mode: "virtual" /* infinite */
        },
        errorRowEnabled: true,
        columnChooser: {
            enabled: COLUMN_CHOOSER,
            mode: "select",
            title: "{{ trans('site.Choose column') }}",
        },
        columnHidingEnabled: true,
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        showBorders: false,
        height: "100%",
        width: "100%",
        allowColumnReordering: false,
        allowColumnResizing: true,
        columnMinWidth: 20,
            visible: true,
        columnResizingMode: 'widget',
        dateSerializationFormat: "yyyy-MM-ddTHH:mm:ss",
        stateStoring: {
            enabled: STATE_STORING,
            type: "sessionStorage",
            storageKey: '{{ $type }}-dx-session-storage'
        },
        selection: {
            mode: "single", /* single,multiple */
        },
        noDataText: "{{ trans('site.No data') }}",
        hoverStateEnabled: true,
        grouping: {
            // autoExpandAll: false
        },
        // searchPanel: {
        //     visible: true,
        //     width: 240,
        //     placeholder: "Search...",
        // },
        // summary: {
        // },
        masterDetail: {
            enabled: MASTER_DETAIL,
            template: function(container, options) {
                master_detail(container, options)
            }
        },
        onToolbarPreparing: function(e) {
            e.toolbarOptions.items.push(
                {
                    location: "before",
                    widget: "dxButton",
                    options: {
                        text: "{!! trans('site.Menu') !!}",
                        icon: "menu",
                        onClick: function(e) {
                            $("#grid-menu").show()
                        }
                    }
                },
                {
                    location: "before",
                    template: function(){
                        return $("<h3/>").html('&nbsp;&nbsp;&nbsp;{{ $title }}')
                    }
                },
                {
                    location: "center",
                    widget: "dxButton",
                    options: {
                        icon: 'email',
                        text: '{{ trans('site.Send message') }}',
                        onClick: function(e) {
                            send_message('parent', RECEIVERS)
                        }
                    }
                },
                {
                    location: "after",
                    widget: "dxButton",
                    options: {
                        icon: 'refresh',
                        onClick: function(e) {
                            dataGrid.clearFilter()
                            dataGrid.filter(FILTER_VALUES)
                            dataGrid.refresh()
                            //e.component.option("disabled", true)
                        }
                    }
                },
            )
        },
        onContextMenuPreparing: function (e) {
            if (e.row && e.row.rowType === "data") {
                e.component.selectRows(e.row.key)
                e.items = []

                e.items.push({
                    html: "<i class='far fa-clock fa-lg'></i> &nbsp;&nbsp; {{ trans('site.Payments') }} &nbsp;&nbsp;",
                    onItemClick: function () {
                        payments_popup(e.row.data.payments)
                    }
                })

                e.items.push({
                    html: "&nbsp;<i class='far fa-file-alt fa-lg'></i> &nbsp;&nbsp; {{ trans('site.Logs') }} &nbsp;&nbsp;",
                    onItemClick: function () {
                        check_logs_popup(e.row.data.check_logs)
                    }
                })

                e.items.push({
                    html: "<hr class='mt-1 mb-1'/>",
                })

                e.items.push({
                    html: "<i class='far fa-edit fa-lg'></i> &nbsp;&nbsp; {{ trans('site.Edit') }} &nbsp;&nbsp;",
                    onItemClick: function () {
                        e.component.editRow(e.row.rowIndex)
                    }
                })

                e.items.push({
                    html: "<i class='far fa-trash-alt fa-lg'></i> &nbsp;&nbsp; {{ trans('site.Delete') }} &nbsp;&nbsp;",
                    onItemClick: function () {
                        e.component.deleteRow(e.row.rowIndex)
                    }
                })
            }
        },
        onContentReady: function(e) {
            RECEIVERS = []
            var items = $.each(e.component.getDataSource().items(), function(index, item) {
                RECEIVERS.push(item.id)
            })
        },
        onOptionChanged: function(e) {
        },
        onRowPrepared: function (row) {
        },
        onEditingStart: function(e) {
        },
        onInitNewRow: function(e) {
        },
        onRowInserting: function(e) {
        },
        onRowInserted: function(e) {
        },
        onRowUpdating: function(e) {
        },
        onRowUpdated: function(e) {
        },
        onRowRemoving: function(e) {
        },
        onRowRemoved: function(e) {
        },
        onRowCollapsed: function(e) {
        },
        onRowExpanding: function(e) {
            e.component.collapseAll(-1);
        },
        onRowExpanded: function(e) {
            e.component.selectRows(e.key)
        },
        onRowClick: function(e) {
        },
        onSelectionChanged: function(e) {
        },
        onCellPrepared: function(e) {
        },
    }).dxDataGrid('instance')
})
</script>