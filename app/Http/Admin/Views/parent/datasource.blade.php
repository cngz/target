<script type="text/javascript">
var SERVICE_URL = '/admin/api/{{ $type }}'

var DATA_SOURCE = new DevExpress.data.CustomStore({
    key: "id",
    loadMode: "raw",
    load: function(loadOptions) {
        return $.ajax({
            url: SERVICE_URL + '/list',
            method: "GET",
            data: {
                filter: loadOptions.filter,
            },
            error: function (request, status, error) {
                error_swal(request)
            }
        })
    },
    insert: function (values) {
        return $.ajax({
            url: SERVICE_URL + '/create',
            method: "POST",
            data: values,
            headers : {'X-CSRF-Token': '{{ csrf_token() }}'},
            error: function (request, status, error) {
                error_swal(request)
            }
        })
    },
    update: function (key, values) {
        return $.ajax({
            url: SERVICE_URL + '/update/' + encodeURIComponent(key),
            method: "POST",
            data: values,
            headers : {'X-CSRF-Token': '{{ csrf_token() }}'},
            error: function (request, status, error) {
                error_swal(request)
            }
        })
    },
    remove: function (key) {
        return $.ajax({
            url: SERVICE_URL + '/delete/' + encodeURIComponent(key),
            method: "POST",
            headers : {'X-CSRF-Token': '{{ csrf_token() }}'},
            error: function (request, status, error) {
                error_swal(request)
            }
        })
    },
})
</script>