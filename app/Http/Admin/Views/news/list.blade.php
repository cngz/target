@extends('Admin::layout-dx')
@section('title', $title)
@section('content')
<div id="grid"></div>
<div id="popup"></div>
@stop
@section('script')
@include('Admin::partials.dx.variables')
@include('Admin::partials.dx.functions')
@include('Admin::partials.dx.dropboxes.driver')
@include('Admin::partials.dx.dropboxes.parent')
@include('Admin::partials.dx.dropboxes.school')
@include('Admin::partials.dx.popups.actions')
@include('Admin::' . $type . '.grid')
@stop