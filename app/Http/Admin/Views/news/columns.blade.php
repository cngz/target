<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "title",
        dataType: "string",
        // visible: false,
        // allowSorting: true,
        // width: 150,
        // alignment: 'left',
        caption: '{{ trans('site.Title') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter title') }}',
        }],
    },
    {
        dataField: "content",
        dataType: "string",
        // allowSorting: true,
        // width: 150,
        // alignment: 'left',
        caption: '{{ trans('site.Message') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter message') }}',
        }],
    },
]
</script>