@extends('Admin::layout')
@section('title', trans('site.Change theme'))
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-sm-6">
            @include('messages.flash')
            <form action="{{ route('admin.auth.theme.update') }}" method="post" class="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <h5>{{ trans('site.Select theme') }}</h5>
                    <select name="theme" class="form-control" required
                    oninvalid="this.setCustomValidity('{{ trans('site.Select theme') }}')"
                    oninput="this.setCustomValidity('')">
                    @foreach($themes as $key => $value)
                    <option value="{{ $key }}" @if($key == auth()->user()->theme) selected @endif>
                        {{ $value }}
                    </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-outline-primary btn-block text-uppercase">
                            <i class="fa fa-check-square"></i> &nbsp; {{ trans('site.Save') }}
                        </button>
                    </div>
                    <div class="col">&nbsp;</div>
                    <div class="col">
                        <a class="btn btn-outline-secondary btn-block text-uppercase" href="{{ route('admin.auth.settings') }}">
                            {{ trans('site.Back') }} &nbsp; <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-6">&nbsp;</div>
</div>
</div>

@stop