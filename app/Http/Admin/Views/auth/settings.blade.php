@extends('Admin::layout')
@section('title', trans('site.Settings'))
@section('content')
<div class="container mt-4">
    <h3 class="text-uppercase">{{ trans('site.Settings') }}</h3>
    <hr>
    <a class="btn btn-outline-primary text-uppercase mr-5" href="{{ route('admin.auth.theme') }}">{{ trans('site.Theme') }}</a>
    <a class="btn btn-outline-primary text-uppercase" href="{{ route('admin.auth.password') }}">{{ trans('site.Change password') }}</a>
</div>
@endsection
