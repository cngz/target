<script type="text/javascript">
function master_detail(container, options) {
    var detailElement = $('<div />')

    detailElement.html('<h4 class="m-4">' + options.data.content + '</h4>')

    detailElement.appendTo(container)
}
</script>