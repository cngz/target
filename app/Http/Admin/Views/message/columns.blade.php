<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "user_id",
        dataType: "string",
        allowSorting: false,
        width: 200,
        caption: '{{ trans('site.Sender') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select sender') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_users()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            user_dropbox(cellElement, cellInfo, get_users())
        },
    },
    {
        dataField: "title",
        dataType: "string",
        // visible: false,
        // allowSorting: true,
        // width: 150,
        // alignment: 'left',
        caption: '{{ trans('site.Title') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter title') }}',
        }],
    },
    {
        dataField: "content",
        dataType: "string",
        // allowSorting: true,
        // width: 150,
        // alignment: 'left',
        caption: '{{ trans('site.Message') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter message') }}',
        }],
    },
    {
        dataField: "type",
        dataType: "string",
        allowSorting: true,
        width: 140,
        // alignment: 'left',
        caption: '{{ trans('site.Receivers') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select receiver type') }}',
        }],
        lookup: {
            dataSource: RECEIVER_TYPES,
            displayExpr: "name",
            valueExpr: "id",
        },
    },
    {
        dataType: "string",
        allowSorting: true,
        caption: '{{ trans("site.Quantity") }}',
        // alignment: "center",
        width: 100,
        calculateCellValue: function(rowData) {
            return rowData.type == 'parent' ? rowData.parents_count : rowData.drivers_count;
        },
    },
    {
        dataField: "create_date",
        dataType: "datetime",
        // allowSorting: false,
        caption: '{{ trans("site.Date") }}',
        // alignment: "center",
        width: 140,
    },
]
</script>