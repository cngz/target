<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "active",
        dataType: "boolean",
        allowSorting: false,
        width: 70,
        alignment: 'center',
        caption: '#',
        lookup: {
            dataSource: USER_STATUSES,
            displayExpr: "name",
            valueExpr: "id",
        },
        cellTemplate: function(container, options) {
            if(options.data && options.data.active == 1)
                $('<i class="far fa-check-square fa-lg fa-dx" style="color: green" />').appendTo(container)
            else
                $('<i class="far fa-square fa-lg fa-dx" style="color: gray" />').appendTo(container)
        },
        editCellTemplate: function(cellElement, cellInfo) {
            return $('<div class="dx-form-size"/>').dxSwitch({
                value: cellInfo.value || cellInfo.value == undefined,
                onText: '{{ trans('site.Active') }}',
                offText: '{{ trans('site.Not active') }}',
                width: '100%',
                onContentReady: function(e) {
                    if(cellInfo.value == undefined)
                        cellInfo.setValue(1)
                },
                onValueChanged: function(item) {
                    cellInfo.setValue(item.value ? 1 : 0)
                },
            }).appendTo(cellElement)
        },
    },
    {
        dataField: "name",
        dataType: "string",
        allowSorting: false,
        minWidth: 260,
        caption: '{{ trans('site.Title') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter title') }}',
        }],
    },
    {
        dataField: "address",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Address') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter address') }}',
        }],
    },
    {
        dataField: "desc",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Notes') }}',
    },
]
</script>