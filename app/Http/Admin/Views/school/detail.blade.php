<script type="text/javascript">
function master_detail(container, options) {
    var detailGridElement = $('<div />')
    var detailGrid =  detailGridElement.dxDataGrid({
        dataSource: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function(loadOptions) {
                return options.data.drivers
            },
        }),
        scrolling: {
            // mode: "virtual" /* infinite */
        },
        // remoteOperations: {
        //     sorting: true,
        //     paging: true,
        //     filtering: true,
        // },
        paging: {
            // pageSize: 10,
            // enabled: true,
        },
        pager: {
            // showPageSizeSelector: true,
            // allowedPageSizes: [10, 20, 50, 100],
            showInfo: true,
        },
        // loadPanel: {
        //     enabled: false,
        // },
        columns: [
            {
                dataField: "passengers",
                dataType: "string",
                allowSorting: false,
                width: 130,
                alignment: 'center',
                caption: '{{ trans('site.Students') }}',
                customizeText: function(data) {
                    return data.value ? data.value.length + '' : '0'
                }
            },
            {
                dataField: "name",
                dataType: "string",
                allowSorting: false,
                minWidth: 260,
                caption: '{{ trans('site.Name') }}',
            },
            {
                dataField: "phone",
                dataType: "string",
                allowSorting: false,
                width: 140,
                caption: '{{ trans('site.Phone') }}',
            },
            {
                dataField: "carno",
                dataType: "string",
                allowSorting: false,
                // alignment: 'center',
                width: 150,
                caption: '{{ trans('site.Car number') }}',
            },
            {
                dataField: "carname",
                dataType: "string",
                allowSorting: false,
                minWidth: 160,
                caption: '{{ trans('site.Car name') }}',
            },
            {
                dataField: "desc",
                dataType: "string",
                allowSorting: false,
                caption: '{{ trans('site.Notes') }}',
            },
        ],
        filterRow: {
            visible: true,
            applyFilter: "auto",
        },
        wordWrapEnabled: true,
        headerFilter: {
            visible: true,
            allowSearch: true,
            height: 400,
            width: 300,
        },
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        // width: "100%",
        // height: "100%",
        allowColumnReordering: false,
        allowColumnResizing: true,
        columnMinWidth: 20,
            visible: true,
        columnResizingMode: 'widget',
        dateSerializationFormat: "yyyy-MM-ddTHH:mm:ss",
        selection: {
            mode: "single", /* multiple */
        },
        noDataText: "{{ trans('site.No data') }}",
        hoverStateEnabled: true,
        errorRowEnabled: true,
        onDataErrorOccurred: function(e) {
        },
        onToolbarPreparing: function(e) {
            e.toolbarOptions.items.push(
                {
                    location: "before",
                    template: function(){
                        return $("<h3/>").html('&nbsp;&nbsp;&nbsp;{{ $subtitle }}')
                    }
                },
            )
        },
    }).dxDataGrid('instance')

    detailGridElement.appendTo(container)
}
</script>