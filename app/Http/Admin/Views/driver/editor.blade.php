<script type="text/javascript">
const DATA_EDITOR = {
    allowAdding: true,
    mode: "popup", /* row, batch, form */
    popup: {
        title: '{{ trans('site.Edit') }}',
        showTitle: true,
        width: "70%",
        height: "70%",
        shading: true,
        shadingColor: "rgba(0, 0, 0, 0.6)",
        position: {
            my: "center",
            at: "center",
            of: window
        }
    },
    form: {
        width: "100%",
        height: "100%",
        elementAttr: {
            'id': 'dx-{{ $type }}-popup-form',
        },
        labelLocation: "top",
        showColonAfterLabel: false,
        colCount: 6,
        items: [
            {
                colSpan: 3,
                dataField: 'name',
                editorOptions: {
                    maxLength: 60,
                }
            },
            {
                colSpan: 3,
                dataField: 'school_id',
            },
            {
                colSpan: 2,
                dataField: 'phone',
                editorOptions: {
                    mask: "000000000",
                    maxLength: 9,
                }
            },
            {
                colSpan: 2,
                dataField: 'carno',
                editorOptions: {
                    maxLength: 20,
                }
            },
            {
                colSpan: 2,
                dataField: 'carname',
                editorOptions: {
                    maxLength: 250,
                }
            },
            {
                colSpan: 6,
                dataField: 'desc',
                editorOptions: {
                    maxLength: 250,
                }
            },
        ]
    },
    texts: CONFIRM_TEXTS,
}
</script>