<!-- Modal -->
<div class="modal fade" id="driver-map-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ trans('site.Select date') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="get" action="{{ route('admin.driver.map', $driver) }}" id="driver-map-form">
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <label>{{ trans('site.Year') }}</label>
                            <select name="year" class="form-control">
                                @for($year = date('Y'); $year > date('Y') - 3; $year--)
                                <option value="{{ $year }}" {{ $year == request('year', date('Y')) ? 'selected' : '' }}>{{ $year }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>{{ trans('site.Month') }}</label>
                            <select name="month" class="form-control">
                                @for($month = 1; $month <= 12; $month++)
                                <option value="{{ $month }}" {{ $month == request('month', date('m')) ? 'selected' : '' }}>{{ month_name($month) }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>{{ trans('site.Day') }}</label>
                            <select name="day" class="form-control">
                                @for($day = 1; $day <= 31; $day++)
                                <option value="{{ $day }}" {{ $day == request('day', date('d')) ? 'selected' : '' }}>{{ $day }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="{{ route('admin.driver.map', $driver) }}" class="btn btn-outline-primary mr-5"><i class="far fa-calendar-alt"></i> {{ trans('site.Today') }}</a>
                <button type="submit" class="btn btn-primary" form="driver-map-form">OK</button>
            </div>
        </div>
    </div>
</div>