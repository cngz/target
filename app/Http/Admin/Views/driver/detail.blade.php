<script type="text/javascript">
function master_detail(container, options) {
    var detailGridElement = $('<div />');

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;

    var detailGrid =  detailGridElement.dxDataGrid({
        dataSource: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function(loadOptions) {
                return options.data.passengers
            },
        }),
        "export": {
            enabled: true,
            fileName: "Пассажиры - " + options.data.name+ "(" + today + ")",
            allowExportSelectedData: false
        },
        scrolling: {
            // mode: "virtual" /* infinite */
        },
        // remoteOperations: {
        //     sorting: true,
        //     paging: true,
        //     filtering: true,
        // },
        paging: {
            // pageSize: 10,
            // enabled: true,
        },
        pager: {
            // showPageSizeSelector: true,
            // allowedPageSizes: [10, 20, 50, 100],
            showInfo: true,
        },
        // loadPanel: {
        //     enabled: false,
        // },
        columns: [
            {
                dataField: "name",
                dataType: "string",
                allowSorting: false,
                // width: 120,
                caption: '{{ trans('site.Name') }}',
            },
            {
                dataField: "phone",
                dataType: "string",
                allowSorting: false,
                width: 140,
                caption: '{{ trans('site.Phone') }}',
            },
            {
                dataField: "parent_id",
                dataType: "string",
                allowSorting: false,
                caption: '{{ trans('site.Address') }}',
                lookup: {
                    dataSource: {
                        key: "id",
                        loadMode: "raw",
                        load: function(loadOptions) {
                            return get_parents()
                        },
                    },
                    searchExpr: ['address'],
                    valueExpr: "id",
                    displayExpr: function(item) {
                        return item && item != undefined ? item.address : ''
                    },
                },
            },
            {
                dataField: "parent_id",
                dataType: "string",
                allowSorting: false,
                caption: '{{ trans('site.Parent') }}',
                lookup: {
                    dataSource: {
                        key: "id",
                        loadMode: "raw",
                        load: function(loadOptions) {
                            return get_parents()
                        },
                    },
                    searchExpr: ['name'],
                    valueExpr: "id",
                    displayExpr: function(item) {
                        return item && item != undefined ? item.name : ''
                    },
                },
            },
            {
                dataField: "desc",
                dataType: "string",
                allowSorting: false,
                hidingPriority: 0,
                // width: 240,
                caption: '{{ trans('site.Notes') }}',
            },
        ],
        filterRow: {
            visible: true,
            applyFilter: "auto",
        },
        wordWrapEnabled: true,
        headerFilter: {
            visible: true,
            allowSearch: true,
            height: 400,
            width: 300,
        },
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        // width: "100%",
        // height: "100%",
        allowColumnReordering: false,
        allowColumnResizing: true,
        columnMinWidth: 20,
            visible: true,
        columnResizingMode: 'widget',
        dateSerializationFormat: "yyyy-MM-ddTHH:mm:ss",
        selection: {
            mode: "single", /* multiple */
        },
        noDataText: "{{ trans('site.No data') }}",
        hoverStateEnabled: true,
        errorRowEnabled: true,
        onDataErrorOccurred: function(e) {
        },
        onToolbarPreparing: function(e) {
            e.toolbarOptions.items.push(
                {
                    location: "before",
                    template: function(){
                        return $("<h3/>").html('&nbsp;&nbsp;&nbsp;{{ $subtitle }}')
                    }
                },
            )
        },
    }).dxDataGrid('instance')

    detailGridElement.appendTo(container)
}
</script>