@extends('Admin::layout-dx')
@section('title', $title)
@section('content')
<div id="grid"></div>
<div id="popup"></div>
@stop
@section('script')
@include('Admin::partials.dx.variables')
@include('Admin::partials.dx.functions')
@include('Admin::partials.dx.menu.datepicker')
@include('Admin::partials.dx.dropboxes.school')
@include('Admin::partials.dx.popups.actions')
@include('Admin::partials.dx.popups.send-message')
@include('Admin::' . $type . '.grid')
@stop
