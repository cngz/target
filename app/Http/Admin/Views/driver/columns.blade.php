<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "passengers_count",
        dataType: "string",
        allowSorting: false,
        width: 130,
        alignment: 'center',
        caption: '{{ trans('site.Students') }}'
    },
    {
        dataField: "name",
        dataType: "string",
        allowSorting: false,
        minWidth: 260,
        caption: '{{ trans('site.Name') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter name') }}',
        }],
    },
    {
        dataField: "phone",
        dataType: "string",
        allowSorting: false,
        width: 140,
        caption: '{{ trans('site.Phone') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter phone number') }}',
        }],
    },
    {
        dataField: "carno",
        dataType: "string",
        allowSorting: false,
        // alignment: 'center',
        width: 150,
        caption: '{{ trans('site.Car number') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter car number') }}',
        }],
    },
    {
        dataField: "carname",
        dataType: "string",
        allowSorting: false,
        minWidth: 160,
        caption: '{{ trans('site.Car name') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter car name') }}',
        }],
    },
    {
        dataField: "school_id",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.School') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select school') }}',
        }],
        lookup: {
            dataSource: {
                key: "id",
                loadMode: "raw",
                load: function(loadOptions) {
                    return get_schools()
                },
            },
            searchExpr: ['name'],
            valueExpr: "id",
            displayExpr: function(item) {
                return item && item != undefined ? item.name : ''
            },
        },
        editCellTemplate: function(cellElement, cellInfo) {
            school_dropbox(cellElement, cellInfo, get_schools())
        },
    },
    {
        dataField: "desc",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Notes') }}',
    },
]
</script>