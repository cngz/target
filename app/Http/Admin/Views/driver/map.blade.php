@extends('Admin::layout-noheader')
@section('title', trans('site.Driver route'))
@section('content')
<div id="map"></div>
<button class="btn btn-secondary btn-sm" id="map-btn" data-toggle="modal" data-target="#driver-map-modal">
    <i class="far fa-calendar-alt"></i>
    <i class="far fa-clock"></i>
</button>
@include('Admin::driver.partials.modal')
@endsection

@section('style')
<style>
    /* Always set the map height explicitly to define the size of the div element that contains the map. */
    #map {
        height: 100%;
    }
    #map-btn {
        position: fixed;
        top: 10px;
        left: 130px;
    }
</style>
@endsection

@section('script')
<script type="text/javascript">
    var markers = []
    var map = {}

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {lat: 42.872778, lng: 74.5857233},
        })

        map.addListener('zoom_changed', function() {
            console.log(map.getZoom())
        })

        addMarker(JSON.parse('{!! $locations !!}'))
    }

    function addMarker(locations) {
        markers = [] /* clear markers */

        var infoWindow = new google.maps.InfoWindow({
            maxWidth: 300,
        })


        google.maps.event.addListener(map, 'click', function () {
            if(infoWindow) infoWindow.close() /* if open close previous infowindow */
        })

        /*Add some markers to the map.*/
        /*Note: The code uses the JavaScript Array.prototype.map() method to*/
        /*create an array of markers based on a given "locations" array.*/
        /*The map() method here has nothing to do with the Google Maps API.*/
        locations.map(function(location, i) {
            var marker = new google.maps.Marker({
                mid     : location.id,
                map     : map,
                title   : location.name,
                position: {lat: parseFloat(location.lat), lng: parseFloat(location.lng)},
            })

            google.maps.event.addListener(marker, 'click', function () {
                var infoHtml = document.createElement('div')
                infoHtml.className  = "cursor-pointer"
                infoHtml.innerHTML += '<strong>'+moment(location.create_date).format('HH:mm:ss')+'</strong>'

                infoWindow.setContent(infoHtml)
                infoWindow.open(map, marker)
            })

            markers[location.id] = marker
        })
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_key') }}&callback=initMap&language=ru&region=KG"></script>
@endsection
