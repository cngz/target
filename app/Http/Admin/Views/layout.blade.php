<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', '') }} - @yield('title')</title>
<link rel="stylesheet" type="text/css" href="{{ mix('/assets/panel.css') }}" />
@yield('style')
</head>
<body>
@include('Admin::partials.header')
@yield('content')
<script type="text/javascript" src="{{ mix('/assets/panel.js') }}"></script>
@yield('script')
</body>
</html>
