@extends('Admin::layout-dx')
@section('title', $title)
@section('content')
<div id="grid"></div>
@stop
@section('script')
@include('Admin::partials.dx.variables')
@include('Admin::partials.dx.functions')
@include('Admin::' . $type . '.grid')
@stop