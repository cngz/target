<script type="text/javascript">
var DATA_COLUMNS = [
    {
        allowSorting: false,
        width: 5,
    },
    {
        dataField: "active",
        dataType: "boolean",
        allowSorting: false,
        width: 70,
        alignment: 'center',
        caption: '#',
        lookup: {
            dataSource: USER_STATUSES,
            displayExpr: "name",
            valueExpr: "id",
        },
        cellTemplate: function(container, options) {
            if(options.data && options.data.active == 1)
                $('<i class="far fa-check-square fa-lg fa-dx" style="color: green" />').appendTo(container)
            else
                $('<i class="far fa-square fa-lg fa-dx" style="color: gray" />').appendTo(container)
        },
        editCellTemplate: function(cellElement, cellInfo) {
            return $('<div class="dx-form-size"/>').dxSwitch({
                value: cellInfo.value || cellInfo.value == undefined,
                onText: '{{ trans('site.Active') }}',
                offText: '{{ trans('site.Not active') }}',
                width: '100%',
                onContentReady: function(e) {
                    if(cellInfo.value == undefined)
                        cellInfo.setValue(1)
                },
                onValueChanged: function(item) {
                    cellInfo.setValue(item.value ? 1 : 0)
                },
            }).appendTo(cellElement)
        },
    },
    {
        dataField: "role",
        dataType: "string",
        allowSorting: false,
        width: 140,
        // visible: false,
        // alignment: "center",
        caption: '{{ trans('site.Role') }}',
        lookup: {
            dataSource: USER_ROLES,
            displayExpr: "name",
            valueExpr: "id",
        },
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Select role') }}',
        }],
    },
    {
        dataField: "name",
        dataType: "string",
        allowSorting: false,
        minWidth: 260,
        caption: '{{ trans('site.Name') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter name') }}',
        }],
    },
    {
        dataField: "phone",
        dataType: "string",
        allowSorting: false,
        width: 140,
        caption: '{{ trans('site.Phone') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter phone number') }}',
        }],
    },
    {
        dataField: "email",
        dataType: "string",
        allowSorting: false,
        width: 240,
        caption: '{{ trans('site.Email') }}',
        validationRules: [{
            type: "required",
            message: '{{ trans('site.Enter email') }}',
        },{
            type: "email",
            message: '{{ trans('site.Enter valid email') }}',
        }],
    },
    {
        dataField: "desc",
        dataType: "string",
        allowSorting: false,
        caption: '{{ trans('site.Notes') }}',
    },
]
</script>