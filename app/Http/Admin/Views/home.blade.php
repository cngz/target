@extends('Admin::layout')
@section('title', trans('site.Management panel'))
@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.school.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-school fa-lg"></i>
                <i class="fa fa-graduation-cap fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Schools') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.driver.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-user-tie fa-lg"></i>
                <i class="fa fa-bus fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Drivers') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.parent.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-female fa-lg"></i>
                <i class="fa fa-male fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Parents') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.student.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-child fa-lg"></i>
                <i class="fa fa-child fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Students') }}</h6>
            </a>
        </div>
        <div class="col-sm-12 form-group"><hr></div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.message.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="far fa-file-alt fa-lg"></i>
                <i class="far fa-edit fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Messages') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.news.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="far fa-newspaper fa-lg"></i>
                <i class="far fa-edit fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.News') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.banner.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="far fa-image fa-lg"></i>
                <i class="far fa-edit fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Banner') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.action.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="far fa-calendar-alt fa-lg"></i>
                <i class="far fa-list-alt fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.History') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.payment.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-credit-card fa-lg"></i>
                <i class="fa fa-calculator fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Payments') }}</h6>
            </a>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.map') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-map-marker-alt fa-lg"></i>
                <i class="fa fa-bus fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Map') }}</h6>
            </a>
        </div>
        <div class="col-sm-12 form-group"><hr></div>
        @if(auth()->user()->isAdmin())
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.user.list') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-user-secret fa-lg"></i>
                <i class="fa fa-user-astronaut fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Managers') }}</h6>
            </a>
        </div>
        @endif
        <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.auth.settings') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-user fa-lg"></i>
                <i class="fa fa-cog fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.Settings') }}</h6>
            </a>
        </div>
        {{-- <div class="col-sm-6 col-md-4 col-lg-3 form-group">
            <a href="{{ route('admin.general.settings') }}" class="btn btn-outline-primary btn-lg btn-block btn-menu-block">
                <i class="fa fa-globe fa-lg"></i>
                <i class="fa fa-cog fa-lg"></i>
                <h6 class="title text-uppercase">{{ trans('site.General settings') }}</h6>
            </a>
        </div> --}}
    </div>
</div>
@stop

@section('style')
<style type="text/css">
.btn-menu-block {
    padding-top: 25px;
}
.btn-menu-block .title {
    margin-top: 15px;
    margin-bottom: 20px;
    overflow: hidden;
}
</style>
@stop