<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(Request $request) {
        $this->middleware(function ($request, $next) {
            if(auth()->user()->isNotAdmin())
                abort(403);

            return $next($request);
        });
    }

    public function list(Request $request) {
        return view('Admin::user.list', [
            'type'     => 'user',
            'title'    => trans('site.Managers'),
            'subtitle' => '',
        ]);
    }
}
