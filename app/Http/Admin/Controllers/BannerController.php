<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;
use Models\Banner\Banner;

class BannerController extends Controller
{
    public function __construct(Request $request) {

    }

    public function list(Request $request) {
        $images = \Storage::disk('public')->files('img/banner');

        foreach ($images as $key=>$image)
        {
            $images[$key] = "storage/".$image;
        }

        rsort($images);
        return view('Admin::banner.list', [
            'type'     => 'banner',
            'title'    => trans('site.Banner'),
            'subtitle' => '',
            'images' => $images
        ]);
    }

    public function delete(Request $request) {

        $name = $request->input('filename');
        $name = str_replace('storage', '', $name);
        \Storage::disk('public')->delete($name);
        return redirect()->route('admin.banner.list');
    }


    public function upload(Request $request) {
        $input = $request->all();
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $dir  = 'img/banner';
            $var = date('YmdHis');
            \Storage::disk('public')->makeDirectory($dir);

            $ext = $file->extension();

            if(in_array($file->extension(), ['png', 'jpg', 'jpeg'])) {
                /* file size 4 mega bytes */
                if($file->getClientSize() < 4 * 1024 * 1024) {
                    $path  = $dir . '/' . $var . '.jpg';

                    $img = \Intervention::make($file)
                        ->interlace()
                        ->crop($input["coord-w"], $input["coord-h"], $input["coord-x"], $input["coord-y"])
                        ->encode('jpg', 80)
                        ->save(storage_path('app/public/' . $path));

                    $img->destroy();

                } else {
                    session()->flash('danger-message', trans('site.Image size is too big'));
                }
            } else {
                session()->flash('danger-message', trans('site.Wrong image type'));
            }

        }

        return redirect()->route('admin.banner.list');
    }
}
