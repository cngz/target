<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

use Models\Student\Student;

class StudentController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::student.list', [
            'type'     => 'student',
            'title'    => trans('site.Students'),
            'subtitle' => '',
        ]);
    }

    public function image(Student $student) {
        return view('Admin::student.image', [
            'student' => $student,
        ]);
    }

    public function upload(Request $request, Student $student) {
        $input = $request->all();

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $dir  = 'img/upload';
            $root = '/storage';

            \Storage::disk('public')->makeDirectory($dir);

            $ext = $file->extension();

            if(in_array($file->extension(), ['png', 'jpg', 'jpeg'])) {
                /* file size 4 mega bytes */
                if($file->getClientSize() < 4 * 1024 * 1024) {
                    $path  = $dir . '/' . $student->uuid . '.jpg';

                    $img = \Intervention::make($file)
                    ->interlace()
                    ->crop($input["coord-w"], $input["coord-h"], $input["coord-x"], $input["coord-y"])
                    ->fit(800, 800)
                    ->encode('jpg', 80)
                    ->save(storage_path('app/public/' . $path));

                    $img->destroy();

                    $student->update([
                        'image'     => asset('storage/' . $path),
                        'signature' => now(),
                    ]);

                } else {
                    session()->flash('danger-message', trans('site.Image size is too big'));
                }
            } else {
                session()->flash('danger-message', trans('site.Wrong image type'));
            }

        }

        return redirect()->route('admin.student.image', $student);
    }
}
