<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class ParentController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::parent.list', [
            'type'     => 'parent',
            'title'    => trans('site.Parents'),
            'subtitle' => trans('site.Children'),
        ]);
    }
}
