<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::message.list', [
            'type'     => 'message',
            'title'    => trans('site.Messages'),
            'subtitle' => '',
        ]);
    }
}
