<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

use Models\School\School;
use Models\Parent\Parentt;
use Models\Driver\Driverr;

use App\Jobs\PushMessageJob;

class HomeController extends Controller
{

    public function home() {
        $driver = Driverr::first();
        $location = [
            'driverid' => $driver->id,
            'lat'      => $driver->lat,
            'lng'      => $driver->lng,
        ];

        return view('Admin::home');
    }

    public function map(Request $request) {
        return view('Admin::map.index');
    }


    public function send_message(Request $request) {
        $type      = $request->get('type', '');
        $title     = $request->get('title');
        $content   = $request->get('content', '');
        $receivers = $request->get('receivers', []);

        $message = auth()->user()->messages()->create([
            'type'    => $type,
            'title'   => $title,
            'content' => $content,
        ]);

        if($type == 'parent') {
            $message->parents()->attach($receivers);
            $receivers = Parentt::whereIn('id', $receivers)->get();
        } elseif($type == 'driver') {
            $message->drivers()->attach($receivers);
            $receivers = Driverr::whereIn('id', $receivers)->get();
        }

        $data['push_title']   = empty($title) ? trans('site.You have new message') : $title;
        $data['push_message'] = $content;
        $data['push_type']    = 'message';

        foreach ($receivers as $receiver) {
            if($receiver->hasDevice()) {
                $data['device_type']  = $receiver->device_type;
                $data['device_token'] = $receiver->device_token;

                $msg = \PushNotification::Message($data['push_title'], [
                        'badge' => 1,
                        'sound' => 'default',
                        'custom' => [
                            'badge'   => 1,
                            'type'    => $data['push_type'],
                            'message' => $data['push_message'],
                        ]
                    ]);

                \PushNotification::app($data['device_type'])
                                 ->to($data['device_token'])
                                 ->send($msg);

                /* push package has issues with jobs */
                // PushMessageJob::dispatch($data)->delay(now()->addSeconds(5));
            }
        }

        return $message;
    }
}
