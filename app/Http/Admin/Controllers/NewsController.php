<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;
use Models\News\News;

class NewsController extends Controller
{
    public function __construct(Request $request) {

    }

    public function list(Request $request) {
        return view('Admin::news.list', [
            'type'     => 'news',
            'title'    => trans('site.News'),
            'subtitle' => '',
        ]);
    }

    public function image(News $news) {
        return view('Admin::news.image', [
            'news' => $news,
        ]);
    }

    public function upload(Request $request, News $news) {
        $input = $request->all();

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $dir  = 'img/news';

            \Storage::disk('public')->makeDirectory($dir);

            $ext = $file->extension();

            if(in_array($file->extension(), ['png', 'jpg', 'jpeg'])) {
                /* file size 4 mega bytes */
                if($file->getClientSize() < 4 * 1024 * 1024) {
                    $path  = $dir . '/' . $news->uuid . '.jpg';

                    $img = \Intervention::make($file)
                        ->interlace()
                        ->crop($input["coord-w"], $input["coord-h"], $input["coord-x"], $input["coord-y"])
                        ->fit(800, 800)
                        ->encode('jpg', 80)
                        ->save(storage_path('app/public/' . $path));

                    $img->destroy();

                    $news->update([
                        'image'     => asset('storage/' . $path),
                    ]);

                } else {
                    session()->flash('danger-message', trans('site.Image size is too big'));
                }
            } else {
                session()->flash('danger-message', trans('site.Wrong image type'));
            }

        }

        return redirect()->route('admin.news.list');
    }
}
