<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::payment.list', [
            'type'     => 'payment',
            'title'    => trans('site.Payments'),
            'subtitle' => '',
        ]);
    }
}
