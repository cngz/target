<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::school.list', [
            'type'     => 'school',
            'title'    => trans('site.Schools'),
            'subtitle' => trans('site.Drivers'),
        ]);
    }
}
