<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\Message\Message;

class MessageController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, Message $message = null) {
        $data['inputs'] = $request->only('user_id', 'title', 'content', 'type');

        $data['rules'] = [];

        if($request->has('user_id')) $data['rules']['user_id']  = 'required|exists:users,id';

        return $data;
    }

    public function list() {
        $messages = Message::with('sender')->withCount('parents', 'drivers')->orderBy('id', 'desc')->get();

        return $messages;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $message = Message::create($data['inputs']);

        return $message;
    }

    public function update(Request $request, Message $message)
    {
        $data      = $this->prepare($request, 'update', $message);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $message->update($data['inputs']);

        return $message;
    }

    public function delete(Message $message)
    {
        $message->delete();

        return $message;
    }
}
