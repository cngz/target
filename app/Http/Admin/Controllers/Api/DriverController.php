<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\Driver\Driverr;

class DriverController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, Driverr $driver = null) {
        $data['inputs'] = $request->only('school_id', 'carno', 'carname', 'name', 'phone', 'desc');

        $data['rules'] = [];

        if($request->has('name'))  $data['rules']['name']  = 'required';
        if($request->has('carno')) $data['rules']['carno'] = 'required|unique:drivers,carno' . ( $mode == 'create' ? '' : ',' . $driver->id);
        if($request->has('phone')) $data['rules']['phone'] = 'required|numeric|unique:drivers,phone' . ( $mode == 'create' ? '' : ',' . $driver->id);

        return $data;
    }

    public function list() {
        $drivers = Driverr::with(['passengers', 'school'])->withCount('passengers')->orderBy('id', 'desc')->get();

        return $drivers;
    }

    public function map(Request $request) {
        $drivers = Driverr::with('school')->where(function($query) use ($request) {
            $ids = data_get($request->all(), 'drivers', []);

            if(empty($ids) == false)
                $query->whereIn('id', $ids);

        })->get();

        return $drivers;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $driver = Driverr::create($data['inputs']);

        return $driver;
    }

    public function update(Request $request, Driverr $driver)
    {
        $data      = $this->prepare($request, 'update', $driver);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $driver->update($data['inputs']);

        return $driver;
    }

    public function delete(Driverr $driver)
    {
        $driver->delete();

        return $driver;
    }
}
