<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\News\News;

class NewsController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, News $news = null) {
        $data['inputs'] = $request->only('title', 'content', 'image');
        $data['rules'] = [];
        return $data;
    }

    public function list() {
        $news = News::orderBy('create_date', 'desc')->get();

        return $news;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $news = News::create($data['inputs']);

        return $news;
    }

    public function update(Request $request, News $news)
    {
        $data      = $this->prepare($request, 'update', $news);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $news->update($data['inputs']);

        return $news;
    }

    public function delete(News $news)
    {
        $news->delete();

        return $news;
    }
}
