<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\Payment\Payment;

class PaymentController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, Payment $payment = null) {
        $data['inputs'] = $request->only('parent_id', 'amount', 'date', 'desc');

        $data['rules'] = [];

        if($request->has('parent_id'))  $data['rules']['parent_id']  = 'required|exists:parents,id';
        if($request->has('amount'))     $data['rules']['amount']     = 'required|numeric';
        if($request->has('date'))       $data['rules']['date']       = 'required|date';

        return $data;
    }

    public function list() {
        $payments = Payment::with('parent')->orderBy('id', 'desc')->get();

        return $payments;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $payment = Payment::create($data['inputs']);

        return $payment;
    }

    public function update(Request $request, Payment $payment)
    {
        $data      = $this->prepare($request, 'update', $payment);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $payment->update($data['inputs']);

        return $payment;
    }

    public function delete(Payment $payment)
    {
        $payment->delete();

        return $payment;
    }
}
