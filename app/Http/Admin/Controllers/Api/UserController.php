<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\User\User;

class UserController extends Controller
{
    public function __construct(Request $request) {
        $this->middleware(function ($request, $next) {
            if(auth()->user()->isNotAdmin())
                abort(403);

            return $next($request);
        });
    }

    public function prepare(Request $request, $mode, User $user = null) {
        $data['inputs'] = $request->only('name', 'phone', 'email', 'role', 'active', 'desc');

        $data['rules'] = [];

        if($request->has('name'))  $data['rules']['name']  = 'required';
        if($request->has('phone')) $data['rules']['phone'] = 'required|numeric|unique:users,phone' . ( $mode == 'create' ? '' : ',' . $user->id);
        if($request->has('email')) $data['rules']['email'] = 'required|email|unique:users,email' . ( $mode == 'create' ? '' : ',' . $user->id);

        return $data;
    }

    public function list() {
        $users = User::orderBy('id', 'desc')->get();

        return $users;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $user = User::create($data['inputs']);

        return $user;
    }

    public function update(Request $request, User $user)
    {
        $data      = $this->prepare($request, 'update', $user);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $user->update($data['inputs']);

        return $user;
    }

    public function delete(User $user)
    {
        $user->delete();

        return $user;
    }
}
