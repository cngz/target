<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\School\School;

class SchoolController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, School $school = null) {
        $data['inputs'] = $request->only('name', 'address', 'active', 'desc');

        $data['rules'] = [];

        if($request->has('name'))  $data['rules']['name']  = 'required';

        return $data;
    }

    public function list() {
        $schools = School::with('drivers.passengers')->orderBy('id', 'desc')->get();
        return $schools;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $school = School::create($data['inputs']);

        return $school;
    }

    public function update(Request $request, School $school)
    {
        $data      = $this->prepare($request, 'update', $school);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $school->update($data['inputs']);

        return $school;
    }

    public function delete(School $school)
    {
        $school->delete();

        return $school;
    }
}
