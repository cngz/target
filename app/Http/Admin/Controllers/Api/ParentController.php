<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\Parent\Parentt;

class ParentController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, Parentt $parent = null) {
        $data['inputs'] = $request->only('name', 'phone', 'fare', 'start_date', 'end_date', 'address', 'desc');

        $data['rules'] = [];

        if($request->has('name'))       $data['rules']['name']       = 'required';
        if($request->has('phone'))      $data['rules']['phone']      = 'required|numeric|unique:parents,phone' . ( $mode == 'create' ? '' : ',' . $parent->id);
        if($request->has('start_date')) $data['rules']['start_date'] = 'date';
        if($request->has('end_date'))   $data['rules']['end_date']   = 'date';

        return $data;
    }

    public function list(Request $request) {
        $parents = Parentt::with('children.school', 'payments', 'check_logs')->withCount('children')->orderBy('id', 'desc')->get();
        return $parents;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $parent = Parentt::create($data['inputs']);

        return $parent;
    }

    public function update(Request $request, Parentt $parent)
    {
        $data      = $this->prepare($request, 'update', $parent);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $parent->update($data['inputs']);

        return $parent;
    }

    public function delete(Parentt $parent)
    {
        $parent->delete();

        return $parent;
    }
}
