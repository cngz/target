<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Models\Student\Student;

class StudentController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, Student $student = null) {
        $data['inputs'] = $request->only('parent_id', 'school_id', 'driver_id', 'name', 'phone', 'desc');

        $data['rules'] = [];

        if($request->has('name'))  $data['rules']['name']  = 'required';
        if($request->has('phone') && !empty($request->get('phone')))
            $data['rules']['phone'] = 'numeric|unique:students,phone' . ( $mode == 'create' ? '' : ',' . $student->id);

        return $data;
    }

    public function list() {
        $students = DB::table('students')->get();
        return $students;
    }


    public function list_actions($id) {
        $actions = DB::table('actions')->where('student_id',$id)->get();
        return $actions;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $student = Student::create($data['inputs']);

        return $student;
    }

    public function update(Request $request, Student $student)
    {
        $data      = $this->prepare($request, 'update', $student);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $student->update($data['inputs']);

        return $student;
    }

    public function delete(Student $student)
    {
        $student->delete();

        return $student;
    }
}
