<?php

namespace App\Http\Admin\Controllers\Api;

use Illuminate\Http\Request;
use Models\Action\Action;

class ActionController extends Controller
{
    public function __construct(Request $request) {

    }

    public function prepare(Request $request, $mode, Action $action = null) {
        $data['inputs'] = $request->only('driver_id', 'student_id', 'type', 'take_date', 'leave_date', 'status');

        $data['rules'] = [];

        if($request->has('driver_id'))  $data['rules']['driver_id']  = 'exists:drivers,id';
        if($request->has('student_id')) $data['rules']['student_id'] = 'exists:students,id';
        if($request->has('type'))       $data['rules']['type']       = 'in:h2s,s2h';
        if($request->has('take_date'))  $data['rules']['take_date']  = 'date';
        if($request->has('leave_date')) $data['rules']['leave_date'] = 'date';

        return $data;
    }

    public function list(Request $request) {
        try {
            $date = $request->has('date') ? \Carbon\Carbon::parse($request->get('date')) : now();
        } catch (\Exception $e) {
            $date = now();
        }

        $actions = Action::with('driver', 'student')
                         ->where(function($query) use ($request) {
                            $driver_id = $request->get('driver');

                            if(empty($driver_id) == false)
                                $query->where('driver_id', $driver_id);
                         })
                         ->where('take_date', '>=', $date->format('Y-m-d 00:00:00'))
                         ->where('take_date', '<=', $date->format('Y-m-d 23:59:59'))
                         ->orderBy('id', 'desc')
                         ->get();

        return $actions;
    }

    public function create(Request $request)
    {
        $data      = $this->prepare($request, 'create');
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $action = Action::create($data['inputs']);

        return $action;
    }

    public function update(Request $request, Action $action)
    {
        $data      = $this->prepare($request, 'update', $action);
        $validator = \Validator::make($data['inputs'], $data['rules']);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data validation error',
                'errors'  => $validator->errors(),
            ], 500);
        }

        $action->update($data['inputs']);

        return $action;
    }

    public function delete(Action $action)
    {
        $action->delete();

        return $action;
    }
}
