<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class ActionController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::action.list', [
            'type'     => 'action',
            'title'    => trans('site.History'),
            'subtitle' => '',
        ]);
    }
}
