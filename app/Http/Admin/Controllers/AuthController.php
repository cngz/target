<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function settings() {
        return view('Admin::auth.settings');
    }

    public function password() {
        return view('Admin::auth.password');
    }

    public function password_update(Request $request) {
        if(!\Hash::check($request->get('current_password', ''), auth()->user()->password))
            return redirect()->route('admin.auth.password')->with('danger-message', trans('site.Incorrect current password'));

        $validator = \Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.auth.password')
                        ->withErrors($validator)
                        ->withInput();
        }

        auth()->user()->update(['password' => bcrypt($request->get('password'))]);

        return redirect()->route('admin.auth.password')->with('success-message', trans('site.Password was changed'));
    }

    public function theme() {
        $themes = [
            "light" => "Light",
            "light.compact" => "Light compact",
            "dark" => "Dark",
            "dark.compact" => "Dark compact",
            "greenmist" => "Green mist",
            "greenmist.compact" => "Green mist compact",
            "darkmoon" => "Dark Moon",
            "darkmoon.compact" => "Dark moon compact",
            "carmine" => "Carmine",
            "carmine.compact" => "Carmine compact",
            "softblue" => "Soft blue",
            "softblue.compact" => "Soft blue compact",
            "darkviolet" => "Dark violet",
            "darkviolet.compact" => "Dark violet compact",
            "contrast" => "Contrast",
            "contrast.compact" => "Contrast compact",
        ];

        return view('Admin::auth.theme', [
            'themes' => $themes,
        ]);
    }

    public function theme_update(Request $request) {
        auth()->user()->update([
            'theme' => $request->get('theme', 'light'),
        ]);

        return redirect()->route('admin.auth.theme')->with('success-message', trans('site.Update was successfull'));
    }
}
