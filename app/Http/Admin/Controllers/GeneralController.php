<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function settings() {
        return view('Admin::general.settings');
    }

    public function location() {
        return view('Admin::general.location');
    }

    public function location_update(Request $request) {
        $validator = \Validator::make($request->only('start_time', 'end_time'), [
            'location' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.general.location')
                        ->withErrors($validator)
                        ->withInput();
        }

        General::updateOrCreate(['start_time' => date('H:i')]);

        return redirect()->route('admin.general.location');
    }


    public function push() {
        return view('Admin::general.push');
    }

    public function push_update(Request $request) {
        $data = $request->only('title', 'message', 'token', 'type');

        $validator = \Validator::make($data, [
            'title'   => 'required',
            'message' => 'required',
            'token'   => 'required',
            'type'    => 'required|in:message,action'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.general.push')->withInput()->withErrors($validator->errors());
        }

        try {
            $msg = \PushNotification::Message($data['title'], [
                'badge' => 1,
                'sound' => 'default',
                'custom' => [
                    'badge'   => 1,
                    'type'    => $data['type'],
                    'title'   => $data['title'],
                    'message' => $data['message'],
                ]
            ]);

            \PushNotification::app('android')->to($data['token'])->send($msg);

        } catch (\Exception $e) {
            session()->flash('danger-message', $e->getMessage());
        }

        return redirect()->route('admin.general.push')->withInput();
    }
}
