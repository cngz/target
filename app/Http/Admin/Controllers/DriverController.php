<?php

namespace App\Http\Admin\Controllers;

use Illuminate\Http\Request;

use Models\Driver\Driverr;

class DriverController extends Controller
{
    public function __construct(Request $request) {
    }

    public function list(Request $request) {
        return view('Admin::driver.list', [
            'type'     => 'driver',
            'title'    => trans('site.Drivers'),
            'subtitle' => trans('site.Students'),
        ]);
    }

    public function map(Request $request, Driverr $driver) {
        $day   = $request->get('day', date('d'));
        $month = $request->get('month', date('m'));
        $year  = $request->get('year', date('Y'));

        if($day > 31 || $day < 1) $day = date('d');
        if($month > 12 || $month < 1) $month = date('m');
        if($year > date('Y') || $year < date('Y') - 3) $year = date('Y');

        $locations = $driver->locations()
                        ->where('create_date', '>=', $year . '-' . $month . '-' . $day . ' 00:00:00')
                        ->where('create_date', '<=', $year . '-' . $month . '-' . $day . ' 23:59:59')
                        ->get();

        return view('Admin::driver.map', [
            'driver'    => $driver,
            'locations' => $locations,
        ]);
    }
}
