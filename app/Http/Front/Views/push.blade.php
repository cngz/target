@extends('Front::layout')

@section('title', 'Push notifications')

@section('content')

<br>

<div class="container">

    <div class="col-sm-offset-2 col-sm-8">
        @include('messages.flash')
    </div>

    <form action="{{ route('front.postpush') }}" method="post" class="form-horizontal">
        {{ csrf_field() }}
        <div class="col-sm-offset-2 col-sm-8">
            <h3>title:</h3>
            <input type="text" name="push_title" value="{{ old('push_title', '') }}" class="form-control" required>
        </div>
        <div class="col-sm-offset-2 col-sm-8">
            <h3>message:</h3>
            <input type="text" name="push_message" value="{{ old('push_message', '') }}" class="form-control" required>
        </div>
        <div class="col-sm-offset-2 col-sm-8">
            <h3>token:</h3>
            <input type="text" name="push_token" value="{{ old('push_token', '') }}" class="form-control" required>
        </div>
        <div class="col-sm-offset-2 col-sm-8">
            <h3>api key (optional, for android):</h3>
            <input type="text" name="api_key" value="{{ old('api_key', '') }}" class="form-control">
        </div>
        <div class="col-sm-offset-2 col-sm-5">
            <h3>type:</h3>
            <select name="push_type" class="form-control" required>
                <option value="news">Новости от Mydom.kg (news)</option>
                <option value="message" @if(old('push_type', '') == 'message') selected @endif>Сообщение от ТСЖ/Мэрии (message)</option>
                <option value="report" @if(old('push_type', '') == 'report') selected @endif>Ответ на заявку от Домкома/ТСЖ (report)</option>
            </select>
        </div>
        <div class="col-sm-3">
            <h3>env:</h3>
            <select name="push_env" class="form-control" required>
                <option value="ios_dev">ios dev</option>
                <option value="ios" @if(old('push_env', '') == 'ios') selected @endif>ios prod</option>
                <option value="android_dev" @if(old('push_env', '') == 'android_dev') selected @endif>android dev</option>
                <option value="android" @if(old('push_env', '') == 'android') selected @endif>android prod</option>
                <option value="android_domkom" @if(old('push_env', '') == 'android_domkom') selected @endif>android domkom</option>
            </select>
        </div>
        <div class="col-sm-12">&nbsp;</div>
        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-primary btn-block">
                отправить
            </button>
        </div>
    </form>
</div>

<br>

@endsection
