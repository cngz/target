<?php
Route::get ('/',               ['as' => 'front.home',   'uses' => 'HomeController@home']);

/* Auth routes */
Route::auth();

/* Captcha */
Route::get ('/captcha/{length?}', ['as' => 'front.captcha', 'uses' => 'CommonController@captcha']);

/* send mail */
Route::get('/mails/test', 'MailController@test');

/* push */
Route::get('/push',      ['as' => 'front.push',     'uses' => 'HomeController@push']);
Route::post('/postpush', ['as' => 'front.postpush', 'uses' => 'HomeController@postpush']);