<?php

namespace App\Http\Front\Controllers;

use Illuminate\Http\Request;
use App\Mail\TestMail;

class MailController extends Controller
{
    public function test(Request $request)
    {
        $send = true;

        if($send)
            \Mail::to('nurchin@gmail.com')->send(new TestMail());

        return new TestMail();
    }

}
