<?php

namespace App\Http\Front\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        return view('Front::home');
    }

    public function push() {
        return view('Front::push');
    }

    public function postpush() {
        try {
            $message = \PushNotification::Message(request('push_title', ''), [
                'badge'  => 1,
                'sound'  => 'default',
                'custom' => [
                    'type'    => request('push_type', ''),
                    'message' => request('push_message', '')
                ]
            ]);

            $custom = [
                'environment' => 'development',
                'apiKey'      => request('api_key', ''),
                'service'     => 'gcm'
            ];

            \PushNotification::app(empty(request('api_key', '')) ? request('push_env', '') : $custom)
                    ->to(request('push_token', ''))
                    ->send($message);

            session()->flash('success-message', 'Сообщение успешно отправлено');

        } catch (\Exception $e) {
            session()->flash('danger-message', $e->getMessage());
        }

        return redirect()->route('front.push')->withInput();
    }
}
