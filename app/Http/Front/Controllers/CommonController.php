<?php

namespace App\Http\Front\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    /**
     * Generates captcha code
     * @param  integer $length [default length of the captcha code]
     * @return mixed           [returns image with the captcha code]
     */
    public function captcha(Request $request, $length = 6)
    {
        if($length < 1) $length = 1;
        if($length > 9) $length = 9;

        $random = rand(pow(10, $length - 1), pow(10, $length) - 1 );
        session()->put('captcha', $random);

        $img = \Intervention::make('img/captcha/texture.jpg');

        /* progressive image */
        $img->interlace();

        $img->text(session('captcha', $random), rand(51,67), rand(13,18), function($font) use ($length) {
            $font->file(public_path().'/img/captcha/courbd-font.ttf');
            $font->size(32-$length);
            $font->color('#888888');
            $font->align('center');
            $font->valign('middle');
            $font->angle(rand(-4,4));
        });

        /* blur image */
        // $img->blur(2);

        /* just add a tone to the image */
        $img->colorize(rand(-20,20), rand(-20,20), rand(-20,20));

        return $img->response();
    }

}
