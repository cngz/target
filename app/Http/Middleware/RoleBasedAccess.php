<?php

namespace App\Http\Middleware;

use Closure;

class RoleBasedAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array  $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if(auth()->check() && in_array(auth()->user()->role, $roles))
            return $next($request);
        else
            return redirect()->route('front.home');
    }
}
