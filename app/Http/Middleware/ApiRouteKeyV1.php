<?php

namespace App\Http\Middleware;

use Closure;

class ApiRouteKeyV1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has(config('app.api.v1.key'))) {
            return $next($request);
        }

        return response('Unauthorized', 401);
    }
}
